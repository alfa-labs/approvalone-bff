﻿namespace OneHanaUtil.Domain.SL
{
    public class ServiceLayer
    {
        public string SessionId { get; set; }
        public string Uri { get; set; }
        public string CompanyDB { get; set; }
        public string UsernameManager { get; set; }
        public string PasswordManager { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UrlFront { get; set; }
        public int Language { get; set; }
        public string ApprovalUsername { get; set; }
        public string ApprovalPassword { get; set; }
    }
}
