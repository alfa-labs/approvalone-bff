﻿using OneHanaUtil.Domain.Hana;
using OneHanaUtil.Domain.SL;

namespace OneHanaUtil.Domain.Configuration
{
    public class Configuration
    {
        public ServiceLayer ServiceLayer { get; set; }
        public HanaDbConnection HanaDbConnection { get; set; }
    }
}
