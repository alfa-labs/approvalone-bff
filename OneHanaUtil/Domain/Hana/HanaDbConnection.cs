﻿namespace OneHanaUtil.Domain.Hana
{
    public class HanaDbConnection
    {
        public string Server { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
    }
}
