﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace OneHana.Interfaces
{
    public interface IOneHanaService
    {
        Task<T> QueryFirst<T>(string sql);
        Task<IEnumerable<T>> Query<T>(string sql);
        Task<int> Execute(string sql);
    }
}
