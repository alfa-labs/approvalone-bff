﻿using Dapper;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using Sap.Data.Hana;
using System;
using OneHanaUtil.Domain.Configuration;
using OneHanaUtil.Domain.Hana;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace OneHana.Services
{
    public class OneHanaService : IOneHanaService
    {
        private string _urlConnection;

        public OneHanaService(IOptions<Configuration> configurations)
        {
            HanaDbConnection cfgFile = configurations.Value.HanaDbConnection;
            _urlConnection = $"Server={cfgFile.Server};UserID={cfgFile.UserID};Password={cfgFile.Password}";
        }

        public async Task<T> QueryFirst<T>(string sql)
        {
            using (var _connection = new HanaConnection(_urlConnection))
            {
                try
                {
                    var result = await _connection.QueryFirstOrDefaultAsync<T>(sql);
                    return result;
                }
                catch (Exception e)
                {
                    throw new Exception("OneHanaService QueryFirst", e);
                }
            }
        }

        public async Task<IEnumerable<T>> Query<T>(string sql)
        {
            using (var _connection = new HanaConnection(_urlConnection))
            {
                try
                {
                    var result = await _connection.QueryAsync<T>(sql);
                    return result;
                }
                catch (Exception e)
                {
                    throw new Exception("OneHanaService Query", e);
                }
            }
        }

        public async Task<int> Execute(string sql)
        {
            using (var _connection = new HanaConnection(_urlConnection))
            {
                try
                {
                    var result = await _connection.ExecuteAsync(sql);
                    return result;
                }

                catch (Exception e)
                {
                    throw new Exception("OneHanaService Execute", e);
                }
            }
        }
    }
}
