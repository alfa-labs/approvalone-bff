using System;
using System.Threading;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Interfaces;
using Coravel.Mailer.Mail.Interfaces;
using Microsoft.Extensions.Hosting;
using NCrontab;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;

namespace ApprovalOneBff.Work
{
    public class WorkerMail : BackgroundService
    {
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private IMailService _mailService;
        private readonly IMailer _mailer;
        private string Schedule => "*/10 * * * * *"; //Runs every 10 seconds

        private ILoggerRepository _logger;

        public WorkerMail(IMailService mailService, ILoggerRepository logger)
        {
            _logger = logger;
            _schedule = CrontabSchedule.Parse(Schedule, new CrontabSchedule.ParseOptions { IncludingSeconds = true });
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _mailService = mailService;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "WorkerMail",
                Method = "ExecuteAsync",
                Message = $"O serviço está iniciando"
            });

            stoppingToken.Register(() => Console.WriteLine("Tarefa de segundo plano está parando."));

            while (!stoppingToken.IsCancellationRequested)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerMail",
                    Method = "ExecuteAsync",
                    Message = $"Executando Worker de Aprovação de Contrato"
                });

                await Process();
                await Task.Delay(10000, stoppingToken);
            }

            await _logger.Logger(new LogB1
            {
                Owner = "WorkerMail",
                Method = "ExecuteAsync",
                Message = $"O serviço está parando"
            });

            /*do
            {
                var now = DateTime.Now;
                var nextrun = _schedule.GetNextOccurrence(now);
                if (now > _nextRun)
                {
                    await _logger.Logger(new LogB1
                    {
                        Owner = "WorkerMail",
                        Method = "ExecuteAsync",
                        Message = $"Executando Worker de Aprovação de Contrato"
                    });

                    await Process();
                    _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);*/
        }

        private async Task<bool> Process()
        {
            try
            {
                var pendent = _mailService.GetPendentMail();
                var listPendent = pendent.Result;

                foreach (var item in listPendent)
                {
                    await _logger.Logger(new LogB1
                    {
                        Owner = "WorkerMail",
                        Method = "Process",
                        Message = $"Enviando e-mail para {item.ApproverName} referente {item.TypeReg} - {item.Id}"
                    });
                    await _mailService.SendMail(item);
                }
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerMail",
                    Method = "Process",
                    Message = $"Erro ao invocar componentes de e-mail {e.Message}"
                });

                throw new Exception("Erro ao invocar componentes de e-mail", e);
            }

            return true;
        }
    }
}