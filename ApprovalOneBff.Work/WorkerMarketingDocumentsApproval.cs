using System;
using System.Threading;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Interfaces;
using Microsoft.Extensions.Hosting;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;

namespace ApprovalOneBff.Work
{
    public class WorkerMarketingDocumentsApproval : BackgroundService
    {
        private IWorkerMarketingDocumentsApprovalService _workerMarketingDocumentsApprovalService;
        private IManagementApprovalService _managementApprovalService;
        private ILoggerRepository _logger;

        public WorkerMarketingDocumentsApproval(IWorkerMarketingDocumentsApprovalService workerMarketingDocumentsApprovalService,
                                        IManagementApprovalService managementApprovalService,
                                        ILoggerRepository logger)
        {
            _logger = logger;
            _workerMarketingDocumentsApprovalService = workerMarketingDocumentsApprovalService;
            _managementApprovalService = managementApprovalService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "WorkerMarketingDocumentsApproval",
                Method = "ExecuteAsync",
                Message = $"O serviço está iniciando."
            });

            stoppingToken.Register(() => Console.WriteLine("Tarefa de segundo plano está parando."));

            while (!stoppingToken.IsCancellationRequested)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerMarketingDocumentsApproval",
                    Method = "ExecuteAsync",
                    Message = $"Executando Worker de Aprovação de Doc.Mkt"
                });

                await Process();
                await Task.Delay(10000, stoppingToken);
            }

            await _logger.Logger(new LogB1
            {
                Owner = "WorkerMarketingDocumentsApproval",
                Method = "ExecuteAsync",
                Message = $"O serviço está parando"
            });
        }

        private async Task<bool> Process()
        {
            try
            {
                var pendent = await _workerMarketingDocumentsApprovalService.GetDocumentsPendent();

                foreach (var item in pendent)
                {
                    var rule = await _managementApprovalService.GetEtapa(item);

                    if (rule != null)
                    {
                        await _logger.Logger(new LogB1
                        {
                            Owner = "WorkerMarketingDocumentsApproval",
                            Method = "Process",
                            Message = $"Regra Encontrada - Id: {item.Id}, CC: {item.CenterCost} "
                        });

                        await _managementApprovalService.Generate(item, rule);
                    }
                    else
                    {
                        await _logger.Logger(new LogB1
                        {
                            Owner = "WorkerMarketingDocumentsApproval",
                            Method = "Process",
                            Message = $"Regra NAO Encontrada - Id: {item.Id}, CC: {item.CenterCost} "
                        });
                    }

                }
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerMarketingDocumentsApproval",
                    Method = "Process",
                    Message = $"Erro ao analisar registros - {e.Message} "
                });

                throw new Exception("Erro ao analisar registros", e);
            }

            return true;
        }
    }
}