using System;
using System.Threading;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Interfaces;
using Microsoft.Extensions.Hosting;
using NCrontab;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;

namespace ApprovalOneBff.Work
{
    public class WorkerContractApproval : BackgroundService
    {
        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private IWorkerApprovalContractService _approvalContractService;
        private IManagementApprovalService _managementApprovalService;
        private ILoggerRepository _logger;
        private string Schedule => "*/20 * * * * *";

        public WorkerContractApproval(IWorkerApprovalContractService approvalContractService,
                                        IManagementApprovalService managementApprovalService,
                                        ILoggerRepository logger)
        {
            _schedule = CrontabSchedule.Parse(Schedule, new CrontabSchedule.ParseOptions { IncludingSeconds = true });
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _logger = logger;

            _approvalContractService = approvalContractService;
            _managementApprovalService = managementApprovalService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "WorkerContractApproval",
                Method = "ExecuteAsync",
                Message = $"O serviço está iniciando."
            });

            stoppingToken.Register(() => Console.WriteLine("Tarefa de segundo plano está parando."));

            while (!stoppingToken.IsCancellationRequested)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerContractApproval",
                    Method = "ExecuteAsync",
                    Message = $"Executando Worker de Aprovação de Contrato"
                });

                await Process();
                await Task.Delay(5000, stoppingToken);
            }

            await _logger.Logger(new LogB1
            {
                Owner = "WorkerContractApproval",
                Method = "ExecuteAsync",
                Message = $"O serviço está parando."
            });

            /*do
            {
                var now = DateTime.Now;
                var nextrun = _schedule.GetNextOccurrence(now);
                if (now > _nextRun)
                {
                    await _logger.Logger(new LogB1
                    {
                        Owner = "WorkerContractApproval",
                        Method = "ExecuteAsync",
                        Message = $"Executando Worker de Aprovação de Contrato"
                    });

                    await Process();
                    _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);*/
        }

        private async Task<bool> Process()
        {
            try
            {
                var pendent = await _approvalContractService.GetContractPendent();

                foreach (var item in pendent)
                {
                    var rule = await _managementApprovalService.GetEtapa(item);

                    if (rule != null)
                    {
                        await _logger.Logger(new LogB1
                        {
                            Owner = "WorkerContractApproval",
                            Method = "Process",
                            Message = $"Regra Encontrada - Id: {item.Id}, CC: {item.CenterCost} "
                        });

                        await _managementApprovalService.Generate(item, rule);
                    }
                    else
                    {
                        await _logger.Logger(new LogB1
                        {
                            Owner = "WorkerContractApproval",
                            Method = "Process",
                            Message = $"Regra NAO Encontrada - Id: {item.Id}, CC: {item.CenterCost} "
                        });
                    }

                }
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerContractApproval",
                    Method = "Process",
                    Message = $"Erro ao analisar registros - {e.Message} "
                });

                throw new Exception("Erro ao analisar registros", e);
            }

            return true;
        }
    }
}