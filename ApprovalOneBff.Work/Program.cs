using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using OneHana.Interfaces;
using OneHana.Services;
using OneServiceLayer.Interface;
using OneServiceLayer.Repository;
using OneServiceLayer.Services.Facade;
using OneHanaUtil.Domain.Configuration;
using Coravel;
using System.IO;

namespace ApprovalOneBff.Work
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public IConfiguration Configuration { get; }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseContentRoot(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location))
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;

                    services.Configure<Configuration>(configuration.GetSection("AppConfig"));
                    services.AddMailer(configuration);

                    services.AddSingleton<ILoggerRepository, LoggerRepository>();
                    services.AddSingleton<IEasyServiceLayerService, EasyServiceLayerService>();
                    services.AddSingleton<IOneHanaService, OneHanaService>();
                    services.AddSingleton<IMailService, MailService>();
                    services.AddSingleton<IManagementApprovalService, ManagementApprovalService>();
                    services.AddSingleton<IWorkerApprovalContractService, WorkerApprovalContractService>();
                    services.AddSingleton<IWorkerMarketingDocumentsApprovalService, WorkerMarketingDocumentsApprovalService>();
                    services.AddHostedService<WorkerMail>();
                    services.AddHostedService<WorkerContractApproval>();

                    services.AddHostedService<WorkerMarketingDocumentsApproval>();
                });
    }
}
