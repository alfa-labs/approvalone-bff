﻿namespace OneServiceLayer.Domain.Login
{
    public class LoginResponse
    {
        public string SessionId { get; set; }
        public int? SessionTimeout { get; set; }
    }
}
