﻿using System;

namespace OneServiceLayer.Domain.LogApproval
{
    public class LogB1
    {
        public string Method { get; set; }
        public string Message { get; set; }
        public string Owner { get; set; }
        public string Request { get; set; }

        public long Hour
        {
            get
            {
                return GetNowHour();
            }
        }

        private int GetNowHour()
        {
            var now = DateTime.Now;
            var hour = now.Hour.ToString();
            var minute = now.Minute.ToString();

            if (hour.Length == 1)
            {
                hour = "0" + hour;
            }

            if (minute.Length == 1)
            {
                minute = "0" + minute;
            }

            var hourWithMinute = hour + minute;

            return Int16.Parse(hourWithMinute);
        }
    
    }
}
