﻿namespace OneServiceLayer.Domain.Authentication
{
    public class Authentication
    {
        public Authentication(string userName, string password, string companyDB, int language)
        {
            UserName = userName;
            Password = password;
            CompanyDB = companyDB;
            Language = language;
        }

        public string UserName { get; }
        public string Password { get; }
        public string CompanyDB { get; }
        public int Language { get; set; }
    }
}
