﻿using Microsoft.AspNetCore.Mvc;
using OneServiceLayer.Domain.Login;
using RestSharp;
using System.Threading.Tasks;

namespace OneServiceLayer.Interface
{
    public interface IEasyServiceLayerService
    {
        Task<IRestResponse<T>> Get<T>(string endPoint, bool forceAuth = false, int retry = 0, bool loginWithManager = false) where T : class;
        Task<IRestResponse<T>> Post<T>(string endPoint, dynamic obj, bool forceAuth = false, int retry = 0, bool loginWithManager = false) where T : class;
        Task<IRestResponse<T>> Put<T>(string endPoint, dynamic obj, bool forceAuth = false, int retry = 0, bool loginWithManager = false) where T : class;
        Task<IRestResponse<T>> Patch<T>(string endPoint, dynamic obj, bool forceAuth = false, int retry = 0, bool loginWithManager = false) where T : class;
        Task<IRestResponse<T>> Delete<T>(string endPoint, bool forceAuth = false, int retry = 0, bool loginWithManager = false) where T : class;
    }
}
