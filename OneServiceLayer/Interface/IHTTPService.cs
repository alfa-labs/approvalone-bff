﻿using RestSharp;
using System.Threading.Tasks;

namespace OneServiceLayer.Interface
{
    public interface IHTTPService
    {
        Task<IRestResponse<T>> Call<T>(string endPoint, Method method, object obj, string uri = null, string sessionId = null) where T : class;
        Task<IRestResponse<T>> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request);
    }
}
