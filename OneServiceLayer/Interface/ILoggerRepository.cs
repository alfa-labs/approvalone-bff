﻿using System.Threading.Tasks;
using OneServiceLayer.Domain.LogApproval;

namespace OneServiceLayer.Interface
{
    public interface ILoggerRepository
    {
        Task Logger(LogB1 logb1);
    }
}
