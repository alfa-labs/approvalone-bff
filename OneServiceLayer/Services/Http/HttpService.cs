using RestSharp;
using OneServiceLayer.Interface;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp.Serializers.NewtonsoftJson;

namespace OneServiceLayer.Services.Http
{
    public class HTTPService : IHTTPService
    {
        private readonly string _cookie = "B1SESSION";
        private readonly string _type = "application/json";
        private readonly string _header = "Content-Type";
        private readonly string _baseUrl = "/b1s/v1";

        public HTTPService(){}

        public async Task<IRestResponse<T>> Call<T>(string endPoint, Method method, object obj = null, string uri = null, string sessionId = null) where T : class
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

            var request = new RestRequest($"{_baseUrl}{endPoint}", method);

            request.AddHeader(_header, _type);
            
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new List<JsonConverter>
                {
                    new StringEnumConverter()
                }
            };

            var json = JsonConvert.SerializeObject(obj,
                jsonSerializerSettings);

            if (obj != null)
                request.AddParameter(_type, json, ParameterType.RequestBody);

            var baseURL = new Uri(uri);

            var client = new RestClient(baseURL) { CookieContainer = new CookieContainer() };

            client.UseNewtonsoftJson();

            if (!string.IsNullOrEmpty(sessionId))
                client.CookieContainer.Add(baseURL, new Cookie(_cookie, sessionId));

            var response = await ExecuteRequestAsync<T>(client, request);

            return response;
        }

        public async Task<IRestResponse<T>> ExecuteRequestAsync<T>(IRestClient client, IRestRequest request)
            => await client.ExecuteAsync<T>(request);
    }
}
