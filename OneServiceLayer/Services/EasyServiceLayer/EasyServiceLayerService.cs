﻿using Microsoft.Extensions.Options;
using RestSharp;
using System;
using System.Net;
using System.Threading.Tasks;
using OneServiceLayer.Interface;
using OneServiceLayer.Domain.Login;
using OneServiceLayer.Domain.Authentication;
using OneServiceLayer.Services.Http;
using OneHanaUtil.Domain.Configuration;
using Newtonsoft.Json;
using OneServiceLayer.Domain.Errors;

namespace OneServiceLayer.Services.Facade
{
    public class EasyServiceLayerService : IEasyServiceLayerService
    {
        private readonly Configuration _configurations;
        private readonly IHTTPService _httpService;

        public EasyServiceLayerService(IOptions<Configuration> configurations)
        {
            _httpService = new HTTPService();
            _configurations = configurations.Value;

            if (string.IsNullOrEmpty(_configurations.ServiceLayer.SessionId))
                Task.WaitAll(Login());

        }

        public async Task<IRestResponse<T>> Delete<T>(string endpoint,
                                                        bool forceAuth = false,
                                                        int retry = 0,
                                                        bool loginWithManager = false) where T : class
        {
            if (loginWithManager)
            {
                Task.WaitAll(Login(true));
            }

            var response = await _httpService.Call<T>(endpoint, Method.DELETE, null, _configurations.ServiceLayer.Uri, _configurations.ServiceLayer.SessionId);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                if (forceAuth)
                {
                    if (retry < 3)
                    {
                        retry += 1;
                        Task.WaitAll(Login());
                        return await Delete<T>(endpoint, true, retry);
                    }
                }

                throw new Exception(FormatMessageError(response.Content));
            }

            if (!isStatusCodeSuccess(response.StatusCode))
            {
                throw new Exception(FormatMessageError(response.Content));
            }

            // Depois de executar a API com o Manager, recupera a sessão com o usuário comum.
            if (loginWithManager)
            {
                Task.WaitAll(Login());
            }

            return response;
        }

        public async Task<IRestResponse<T>> Get<T>(string endpoint, bool forceAuth = false,
                                                    int retry = 0, bool loginWithManager = false) where T : class
        {
            if (loginWithManager)
            {
                Task.WaitAll(Login(true));
            }

            var response = await _httpService.Call<T>(endpoint, Method.GET, null, _configurations.ServiceLayer.Uri, _configurations.ServiceLayer.SessionId);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                if (forceAuth)
                {
                    if (retry < 3)
                    {
                        retry += 1;
                        Task.WaitAll(Login());
                        return await Get<T>(endpoint, true, retry);
                    }
                }
                throw new Exception(FormatMessageError(response.Content));
            }

            if (response.StatusCode == HttpStatusCode.BadGateway)
            {
                if (retry < 10)
                {
                    retry += 1;
                    return await Get<T>(endpoint, forceAuth, retry);
                }
            }

            if (!isStatusCodeSuccess(response.StatusCode))
            {
                throw new Exception(FormatMessageError(response.Content));
            }

            // Depois de executar a API com o Manager, recupera a sessão com o usuário comum.
            if (loginWithManager)
            {
                Task.WaitAll(Login());
            }

            return response;
        }

        public async Task<IRestResponse<T>> Patch<T>(string endpoint,
                                                        dynamic obj,
                                                        bool forceAuth = false,
                                                        int retry = 0,
                                                        bool loginWithManager = false) where T : class
        {
            if (loginWithManager)
            {
                Task.WaitAll(Login(true));
            }

            var response = await _httpService.Call<T>(endpoint,
                                                        Method.PATCH,
                                                        obj,
                                                        _configurations.ServiceLayer.Uri,
                                                        _configurations.ServiceLayer.SessionId);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                if (forceAuth)
                {
                    if (retry < 3)
                    {
                        retry += 1;
                        Task.WaitAll(Login());
                        return await Patch<T>(endpoint, obj, true, retry);
                    }
                }
                throw new Exception(FormatMessageError(response.Content));
            }

            if (response.StatusCode == HttpStatusCode.BadGateway)
            {
                if (retry < 10)
                {
                    retry += 1;
                    return await Patch<T>(endpoint, obj, forceAuth, retry);
                }
            }

            if (!isStatusCodeSuccess(response.StatusCode))
            {
                throw new Exception(FormatMessageError(response.Content));
            }

            // Depois de executar a API com o Manager, recupera a sessão com o usuário comum.
            if (loginWithManager)
            {
                Task.WaitAll(Login());
            }

            return response;
        }

        public async Task<IRestResponse<T>> Post<T>(string endpoint,
                                                    dynamic obj,
                                                    bool forceAuth = false,
                                                    int retry = 0,
                                                    bool loginWithManager = false) where T : class
        {
            if (loginWithManager)
            {
                Task.WaitAll(Login(true));
            }

            var response = await _httpService.Call<T>(endpoint, Method.POST, obj, _configurations.ServiceLayer.Uri, _configurations.ServiceLayer.SessionId);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                if (endpoint.ToLower().IndexOf("login") < 0)
                {
                    if (forceAuth)
                    {
                        if (retry < 3)
                        {
                            retry += 1;
                            return await Post<T>(endpoint, obj, forceAuth, retry);
                        }
                    }
                }
                throw new Exception(FormatMessageError(response.Content));
            }

            if (response.StatusCode == HttpStatusCode.BadGateway)
            {
                if (retry < 10)
                {
                    retry += 1;
                    return await Post<T>(endpoint, obj, true, retry);
                }
            }

            if (!isStatusCodeSuccess(response.StatusCode))
            {
                throw new Exception(FormatMessageError(response.Content));
            }

            // Depois de executar a API com o Manager, recupera a sessão com o usuário comum.
            if (loginWithManager)
            {
                Task.WaitAll(Login());
            }

            return response;
        }

        public async Task<IRestResponse<T>> Put<T>(string endpoint,
                                                    dynamic obj,
                                                    bool forceAuth = false,
                                                    int retry = 0,
                                                    bool loginWithManager = false) where T : class
        {
            if (loginWithManager)
            {
                Task.WaitAll(Login(true));
            }

            var response = await _httpService.Call<T>(endpoint, Method.PUT, null, _configurations.ServiceLayer.Uri, _configurations.ServiceLayer.SessionId);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                if (forceAuth)
                {
                    if (retry < 3)
                    {
                        retry += 1;
                        Task.WaitAll(Login());
                        return await Put<T>(endpoint, obj, true, retry);
                    }
                }
                throw new Exception(FormatMessageError(response.Content));
            }

            if (!isStatusCodeSuccess(response.StatusCode))
            {
                throw new Exception(FormatMessageError(response.Content));
            }

            // Depois de executar a API com o Manager, recupera a sessão com o usuário comum.
            if (loginWithManager)
            {
                Task.WaitAll(Login());
            }

            return response;
        }

        private async Task<IRestResponse<LoginResponse>> Login(bool withManager = false)
        {
            try
            {
                string userName = _configurations.ServiceLayer.Username; ;
                string passWord = _configurations.ServiceLayer.Password; ;

                if (withManager || (String.IsNullOrEmpty(userName) && String.IsNullOrEmpty(passWord)))
                {
                    userName = _configurations.ServiceLayer.UsernameManager;
                    passWord = _configurations.ServiceLayer.PasswordManager;
                }

                if (!(String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(passWord)))
                {
                    await _httpService.Call<dynamic>("/Logout", Method.POST, null, _configurations.ServiceLayer.Uri, null);

                    var response = await _httpService.Call<LoginResponse>("/Login", Method.POST,
                new Authentication(userName,
                                   passWord,
                                   _configurations.ServiceLayer.CompanyDB,
                                   _configurations.ServiceLayer.Language),
                _configurations.ServiceLayer.Uri,
                _configurations.ServiceLayer.SessionId);

                    if (response.StatusCode == HttpStatusCode.BadGateway)
                    {
                        throw new Exception("Erro BadGateway ao tentar fazer login na ServiceLayer");
                    }

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var sessionId = response.Data.SessionId;
                        _configurations.ServiceLayer.SessionId = sessionId;
                    }
                    return response;
                }

                throw new Exception("Usuário ou Senha não informado no AppConfig");
            }
            catch (Exception e)
            {
                throw new Exception(e.ToString());
            }
        }

        private static string FormatMessageError(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return "Ocorreu um erro interno";

            return JsonConvert.DeserializeObject<ErrorServiceLayer>(message).Error.Message.Value;
        }

        private bool isStatusCodeSuccess(HttpStatusCode statusCode)
        {
            return statusCode == HttpStatusCode.OK ||
                    statusCode == HttpStatusCode.NoContent ||
                    statusCode == HttpStatusCode.Created ||
                    statusCode == HttpStatusCode.Accepted ||
                    statusCode == HttpStatusCode.NotFound;
        }
    }
}
