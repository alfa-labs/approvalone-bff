using ApprovalOneBff.Services.DTO;
using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Services;
using AutoMapper;
using Manager.API.ViewModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OneHana.Interfaces;
using OneHana.Services;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Interface;
using OneServiceLayer.Repository;
using OneServiceLayer.Services.Facade;
using OneServiceLayer.Services.Http;

namespace ApprovalOneBff
{
  public class Startup
  {
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.Configure<Configuration>(Configuration.GetSection("AppConfig"));

      services.AddSingleton<ILoggerRepository, LoggerRepository>();
      services.AddSingleton<IHTTPService, HTTPService>();
      services.AddSingleton<IEasyServiceLayerService, EasyServiceLayerService>();
      services.AddSingleton<IAuthenticationService, AuthenticationService>();
      services.AddSingleton<IApprovalService, ApprovalService>();
      services.AddSingleton<IOneHanaService, OneHanaService>();
      services.AddSingleton<IStructureService, StructureService>();
      services.AddSingleton<IPurchaseRequest, PurchaseRequestService>();
      services.AddSingleton<IBranchService, BranchService>();

      #region AutoMapper

      var autoMapperConfig = new MapperConfiguration(cfg =>
      {
        cfg.CreateMap<ApprovalViewModel, ApprovalDTO>().ReverseMap();
        cfg.CreateMap<LoginViewModel, LoginDTO>().ReverseMap();
      });

      services.AddSingleton(autoMapperConfig.CreateMapper());

      #endregion

      services.AddCors();

      services.AddControllers();
      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "ApprovalOneBff", Version = "v1.1" });
      });

      services.AddSwaggerGenNewtonsoftSupport();
      services.AddControllers().AddNewtonsoftJson();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ApprovalOneBff v1"));
      }

      //app.UseHttpsRedirection();

      app.UseRouting();
      app.UseCors(x => x
          .AllowAnyOrigin()
          .AllowAnyMethod()
          .AllowAnyHeader());

      //app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
