﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OneServiceLayer.Interface;
using ApprovalOneBff.Services.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Net.Mime;
using OneServiceLayer.ViewModels;
using Manager.API.ViewModels;
using AutoMapper;
using ApprovalOneBff.Services.DTO;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;
using OneServiceLayer.Domain.LogApproval;
using Newtonsoft.Json;

namespace ApprovalOneBff.Api.Controllers
{
    [Route("/api/v1/approvals")]
    [ApiController]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class ApprovalController : ControllerBase
    {
        private readonly IMapper _mapper;
        private ILoggerRepository _logger;
        public IApprovalService _approvalService;

        public ApprovalController(ILoggerRepository logger, IMapper mapper, IApprovalService approvalService)
        {
            _logger = logger;
            _approvalService = approvalService;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetApprovals(long id,
                                                    [FromQuery(Name = "type")] string type,
                                                    [FromQuery(Name = "onlyPending")] string onlyPending,
                                                    [FromQuery(Name = "recordId")] long recordId
                                                    )
        {

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Início - Parametros type: { type }, onlyPending: { onlyPending }, recordId: { recordId }"
            });

            var listApproval = await _approvalService.GetApprovalsByUserId(id, type, onlyPending, recordId);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Fim - Parametros type: { type }, onlyPending: { onlyPending }, recordId: { recordId }"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = listApproval
            });
        }

        [HttpGet("detail")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetApprovalDetail([FromQuery(Name = "userId")] long userId, [FromQuery(Name = "type")] string type, [FromQuery(Name = "recordId")] long recordId)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovalDetail",
                Message = $"Início - Parametros type: { type }, recordId: { recordId }"
            });

            var approvalDetail = await _approvalService.GetApprovalDetail(userId, type, recordId);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovalDetail",
                Message = $"Fim - Parametros type: { type }, recordId: { recordId }"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = approvalDetail
            });
        }

        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> ApproveOrReprove(long id, [FromBody] ApprovalViewModel approvalViewModel)
        {
            string jsonSerialized = JsonConvert.SerializeObject(approvalViewModel);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "ApproveOrReprove",
                Message = $"Início - Parametros id: { id } ",
                Request = jsonSerialized
            });

            var userDTO = _mapper.Map<ApprovalDTO>(approvalViewModel);

            var result = await _approvalService.ApproveOrReprove(id, userDTO);

            if (result.Status)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "ApprovalController",
                    Method = "ApproveOrReprove",
                    Message = $"Fim - Aprovação/Rejeição Finalizada com sucesso: { id } "
                });

                return Ok(new ResultViewModel
                {
                    Message = "Sucesso",
                    Success = result.Status,
                    Data = result.Data
                });
            }

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "ApproveOrReprove",
                Message = $"Fim - Erro na Aprovação/Rejeição: { id } {result.Data}"
            });

            return Unauthorized(new ResultViewModel
            {
                Message = result.Data,
                Success = result.Status,
                Data = null
            });
        }

        [HttpGet("totalizers/{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetTotalizers(long id, [FromQuery(Name = "onlyPending")] string onlyPending)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetTotalizers",
                Message = $"Início - Parametros id: { id }, onlyPending: {onlyPending} "
            });

            var totalizers = await _approvalService.GetTotalizers(id, onlyPending);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetTotalizers",
                Message = $"Fim - Parametros id: { id }, onlyPending: {onlyPending} "
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = totalizers
            });
        }

        [HttpGet("attachment/{id}/{line}"), DisableRequestSizeLimit]
        public async Task<IActionResult> GetAttachmentDetail(string id, string line)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetAttachmentDetail",
                Message = $"Início - Parametros id: { id }, line: {line} "
            });

            var approvalDetail = await _approvalService.GetAttachmentDetail(id, line);

            if (!System.IO.File.Exists(approvalDetail.fullpath))
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "ApprovalController",
                    Method = "GetAttachmentDetail",
                    Message = $"Fim - Anexos não encontrados id: { id }, line: {line} "
                });
                return NotFound();
            }

            var memory = new MemoryStream();

            await using (var stream = new FileStream(approvalDetail.fullpath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;
            var response = File(memory, GetContentType(approvalDetail.fullpath), approvalDetail.fullpath);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetAttachmentDetail",
                Message = $"Fim - Parâmetros id: { id }, line: {line} "
            });

            return response;
        }

        private string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;

            if (!provider.TryGetContentType(path, out contentType))
            {
                contentType = "application/octet-stream";
            }

            return contentType;
        }
    }
}