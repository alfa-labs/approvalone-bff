﻿using Microsoft.AspNetCore.Mvc;
using OneServiceLayer.Interface;
using ApprovalOneBff.Services.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Net.Mime;
using OneServiceLayer.ViewModels;
using AutoMapper;
using OneServiceLayer.Domain.LogApproval;

namespace ApprovalOneBff.Api.Controllers
{
  [Route("/api/v1/branchs")]
  [ApiController]
  [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
  public class BranchController : ControllerBase
  {
    private readonly IMapper _mapper;
    private ILoggerRepository _logger;
    private IBranchService _branchService;


    public BranchController(ILoggerRepository logger, IMapper mapper, IBranchService branchService)
    {
      _logger = logger;
      _branchService = branchService;
      _mapper = mapper;
    }

    [HttpGet("")]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult> GetbranchsByUser([FromQuery(Name = "userCode")] string userCode)
    {

      await _logger.Logger(new LogB1
      {
        Owner = "BranchController",
        Method = "GetbranchsByUser",
        Message = $"Início - Parametros type: { userCode } "
      });

      var listBranchs = await _branchService.GetQueryBranchsByUser(userCode);

      await _logger.Logger(new LogB1
      {
        Owner = "BranchController",
        Method = "GetbranchsByUser",
        Message = $"Final - Parametros type: { userCode } "
      });

      return Ok(new ResultViewModel
      {
        Message = "Sucesso",
        Success = true,
        Data = listBranchs
      });
    }
  }
}