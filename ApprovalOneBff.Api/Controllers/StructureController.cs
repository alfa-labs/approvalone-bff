using ApprovalOneBff.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OneServiceLayer.Utilities;
using System;
using System.Net.Mime;
using System.Threading.Tasks;

namespace ApprovalOneBff.Controllers
{
    [ApiController]
    [Route("/api/v1/structure")]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class StructureController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IStructureService _structureService;

        public StructureController(IStructureService structureService, IMapper mapper)
        {
            _structureService = structureService;
            _mapper = mapper;
        }

        [HttpGet("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Structure()
        {
            try
            {
                var result = await _structureService.Structure();
                return Ok(result);

            } catch(Exception e)
            {
                return StatusCode(500, Responses.DomainErrorMessage(e.Message));
            }
        }
    }
}
