﻿using ApprovalOneBff.Services.DTO;
using ApprovalOneBff.Services.Interfaces;
using AutoMapper;
using Manager.API.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;
using OneServiceLayer.Utilities;
using System;
using System.Net.Mime;
using System.Threading.Tasks;

namespace ApprovalOneBff.Controllers
{
    [ApiController]
    [Route("/api/v1/authentication")]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class AuthenticationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILoggerRepository _logger;

        public AuthenticationController(IAuthenticationService authenticationService, IMapper mapper, ILoggerRepository logger)
        {
            _logger = logger;
            _authenticationService = authenticationService;
            _mapper = mapper;
        }

        [HttpPost("login")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginViewModel)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "AuthenticationController",
                Method = "Login",
                Message = $"Início - Parametros Username: { loginViewModel.UserName }, companyDb: { loginViewModel.CompanyDb }"
            });

            try
            {
                var loginDTO = _mapper.Map<LoginDTO>(loginViewModel);
                var loginResult = await _authenticationService.Login(loginDTO);

                if (loginResult.Success)
                {
                    await _logger.Logger(new LogB1
                    {
                        Owner = "AuthenticationController",
                        Method = "Login",
                        Message = $"Login realizado com sucesso - Parametros Username: { loginViewModel.UserName }, companyDb: { loginViewModel.CompanyDb }"
                    });
                    return StatusCode(200, loginResult);
                }

                await _logger.Logger(new LogB1
                {
                    Owner = "AuthenticationController",
                    Method = "Login",
                    Message = $"Dados inválidos: { loginViewModel.UserName }, companyDb: { loginViewModel.CompanyDb }"
                });

                return StatusCode(401, loginResult);

            }
            catch(Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "AuthenticationController",
                    Method = "Login",
                    Message = $"Erro interno: { loginViewModel.UserName } {e.Message}"
                });

                return StatusCode(500, Responses.ApplicationErrorMessage());
            }
        }
    }
}
