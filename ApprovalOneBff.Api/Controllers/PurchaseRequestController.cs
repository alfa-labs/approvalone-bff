using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OneServiceLayer.Interface;
using ApprovalOneBff.Services.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Net.Mime;
using OneServiceLayer.ViewModels;
using AutoMapper;
using OneServiceLayer.Domain.LogApproval;
using ApprovalOneBff.Services.Models;

namespace ApprovalOneBff.Api.Controllers
{

    [ApiController]
    [Route("/api/v1/purchaseRequest")]
    [ResponseCache(NoStore = true, Location = ResponseCacheLocation.None)]
    public class PurchaseRequestController : ControllerBase
    {
        private readonly IMapper _mapper;
        private ILoggerRepository _logger;
        public IConfiguration _configuration { get; }
        public IPurchaseRequest _purchaseRequest;
        public PurchaseRequestController(ILoggerRepository logger, IMapper mapper, IPurchaseRequest purchaseRequest)
        {
            _logger = logger;
            _purchaseRequest = purchaseRequest;
            _mapper = mapper;
        }

        [HttpGet("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetAllPurchaseRequest([FromQuery(Name = "requester")] string requester, [FromQuery(Name = "docNumber")] string docNumber)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Início - Processo Get"
            });

            var listPurchaseRequest = await _purchaseRequest.GetAllPurchaseRequest(requester, docNumber);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Fim - Processo Get"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = listPurchaseRequest
            });
        }

        [HttpGet("{code}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetSpecificPurchaseRequest(string code)
        {

            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Início - Processo Get Specific {code}"
            });

            var purchaseRequest = await _purchaseRequest.GetSpecificPurchaseRequest(code);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Fim - Processo Get Specific {code}"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = purchaseRequest
            });
        }

        [HttpPost("")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> PostPurchaseRequest([FromBody] PurchaseRequest objpurchaseRequest)
        {

            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Início - Processo Post Purchase Request"
            });

            var listPurchaseRequest = await _purchaseRequest.PostPurchaseRequest(objpurchaseRequest);

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Fim - Processo Post Purchase Request"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = listPurchaseRequest
            });
        }
        [HttpPatch("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> PatchPurchaseRequest(string id, PurchaseRequest objpurchaseRequest)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Início - Processo Patch DocNum: {id}"
            });

            await _purchaseRequest.PatchPurchaseRequest(id, objpurchaseRequest);

            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Fim - Processo Patch DocNum:{id}"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = { }
            });
        }

        [HttpGet("GetItems")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetItems()
        {

            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Início - Processo Get Specific"
            });

            var listItems = await _purchaseRequest.GetItems();

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Fim - Processo Get Specific"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = listItems
            });
        }

        [HttpGet("GetCostCenter")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> GetCostCenter()
        {

            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "GetAllPurchaseRequest",
                Message = $"Início - Processo Get Specific"
            });

            var listCostCenter = await _purchaseRequest.GetCostCenter();

            await _logger.Logger(new LogB1
            {
                Owner = "ApprovalController",
                Method = "GetApprovals",
                Message = $"Fim - Processo Get Specific"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = listCostCenter
            });
        }

        [HttpPost("Cancel/{code}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult> CancelPurchaseRequest(string code)
        {
            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "CancelPurchaseRequest",
                Message = $"Início - {code}"
            });

            await _purchaseRequest.CancelPurchaseRequest(code);

            await _logger.Logger(new LogB1
            {
                Owner = "PurchaseRequestController",
                Method = "CancelPurchaseRequest",
                Message = $"Fim - {code}"
            });

            return Ok(new ResultViewModel
            {
                Message = "Sucesso",
                Success = true,
                Data = { }
            });
        }
    }
}