﻿using System.ComponentModel.DataAnnotations;

namespace Manager.API.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "O usuário não pode ser vazio.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "A senha não pode ser vazio.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "O database não pode ser vazio.")]
        public string CompanyDb { get; set; }

    }
}