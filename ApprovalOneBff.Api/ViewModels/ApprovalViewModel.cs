using System.ComponentModel.DataAnnotations;

namespace Manager.API.ViewModels
{
    public class ApprovalViewModel
    {
        [Required(ErrorMessage = "O ID não pode ser vazio.")]
        public long Id { get; set; }

        [Required(ErrorMessage = "A status de aprovação deve ser informado.")]
        public bool Approved { get; set; }
        public string Remarks { get; set; }
        public string LineNum { get; set; }
        public string Type { get; set; }
        public long UserId { get; set; }
        public int Sequence { get; set; }

    }
}