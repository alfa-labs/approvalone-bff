﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Models;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IWorkerMarketingDocumentsApprovalService
    {
        Task<IEnumerable<DocumentsApprovalFlow>> GetDocumentsPendent();
    }
}
