﻿using ApprovalOneBff.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IWorkerApprovalContractService
    {
        Task<IEnumerable<DocumentsApprovalFlow>> GetContractPendent();   
    }
}
