using System.Collections.Generic;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Models;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IManagementApprovalService
    {
        Task<bool> Generate(DocumentsApprovalFlow item, IEnumerable<Rule> etapas);
        Task<IEnumerable<Rule>> GetEtapa(DocumentsApprovalFlow item);  
    }
}