using ApprovalOneBff.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IPurchaseRequest
    {
        Task<IEnumerable<PurchaseRequest>> GetAllPurchaseRequest(string requester, string docNumber);
        Task<PurchaseRequest> GetSpecificPurchaseRequest(string id);
        Task<dynamic> PostPurchaseRequest(PurchaseRequest purchaseRequest);
        Task<dynamic> PatchPurchaseRequest(string id, PurchaseRequest purchaseRequest);
        Task<IEnumerable<Items>> GetItems();
        Task<IEnumerable<CostCenter>> GetCostCenter();
        Task<dynamic> CancelPurchaseRequest(string code);
    }
}
