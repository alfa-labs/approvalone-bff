using ApprovalOneBff.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IMailService
    {
        Task<IEnumerable<Mail>> GetPendentMail();
        Task<bool> SendMail(Mail mail);
    }
}
