﻿using ApprovalOneBff.Services.DTO;
using OneServiceLayer.ViewModels;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IAuthenticationService
    {
        public Task<ResultViewModel> Login(LoginDTO loginDto);
    }
}
