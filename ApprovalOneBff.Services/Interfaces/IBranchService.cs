using ApprovalOneBff.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
  public interface IBranchService
  {
    Task<IEnumerable<Branch>> GetQueryBranchsByUser(string userCode);
  }
}
