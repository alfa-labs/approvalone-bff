﻿using ApprovalOneBff.Services.DTO;
using ApprovalOneBff.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IApprovalService
    {
        Task<IEnumerable<Header>> GetApprovalsByUserId(long userId, string type, string onlyPendings, long recordId = 0);
        Task<IEnumerable<Totalizers>> GetTotalizers(long id, string onlyPendings);
        Task<ResultApiServiceLayer> ApproveOrReprove(long id, ApprovalDTO approvalDTO);
        Task<ApprovalDetail> GetApprovalDetail(long userId, string type, long recordId = 0);
        Task<AttachmentInfo> GetAttachmentDetail(string attachmentId, string line);
    }
}
