using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Interfaces
{
    public interface IStructureService
    {
        Task<dynamic> Structure();
    }
}
