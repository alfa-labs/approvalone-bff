using System;

namespace ApprovalOneBff.Services.Services
{
    public class PurchaseRequestQueries
    {
        private string _hanadb;

        public PurchaseRequestQueries(string hanadb)
        {
            _hanadb = hanadb;
        }
        internal string GetPurchaseRequestList(string requester, string docNumber)
        {
            string query = $@"SELECT
                            IFNULL(T0.""DocNum"",0) ""DocNum""
                            , IFNULL(T0.""DocEntry"",0) ""DocEntry""
                            , IFNULL(T0.""DocStatus"",'') ""DocStatus""
                            , IFNULL(T0.""Comments"",'') ""Comments""
                            , IFNULL(T0.""Requester"",'') ""Requester""
                            , IFNULL(T0.""ReqName"",'')  ""ReqName""
                            , T0.""BPLId"" ""BPLId""
                            , TO_NVARCHAR(T0.""ReqDate"" , 'DD-MM-YYYY') ""ReqDate""
                            , TO_NVARCHAR(T0.""DocDate"" , 'DD-MM-YYYY') ""DocDate""
                            , TO_NVARCHAR(T0.""TaxDate"" , 'DD-MM-YYYY') ""TaxDate""
                            , TO_NVARCHAR(T0.""DocDueDate"" , 'DD-MM-YYYY') ""DocDueDate""
                            FROM ""{_hanadb}"".OPRQ T0
                            WHERE T0.""CANCELED"" = 'N'
                            AND T0.""Requester"" = '{requester}' ";

            if (!String.IsNullOrEmpty(docNumber))
            {
                query += $@"AND T0.""DocNum"" = '{docNumber}'";
            }


            query += $@"ORDER BY ""DocNum"" DESC ";

            return query;
        }

        internal string GetPurchaseRequestSpecific(string id)
        {
            string query =
                        $@"
                        SELECT 
                            T0.""DocNum""
                            , TO_NVARCHAR(T0.""DocDate"", 'DD-MM-YYYY') ""DocDate""
                            , TO_NVARCHAR(T0.""DocDueDate"", 'DD-MM-YYYY') ""DocDueDate""
                            , T0.""CardCode""
                            , T0.""CardName""
                            , T0.""DocTime""
                            , CAST(T0.""BPLId"" AS INT) ""BPL_IDAssignedToInvoice""
                            , T1.""ItemCode""
                            , T1.""Dscription""
                            , T1.""Quantity""
                            , T1.""Price""
                            , T0.""Comments"" 
                            , TO_NVARCHAR(T0.""ReqDate"", 'DD-MM-YYYY') ""ReqDate""
                            , ""T2"".""BPLName"" ""branchName""
                        FROM ""{_hanadb}"".OPRQ T0
                        INNER JOIN ""{_hanadb}"".PRQ1 T1 ON T0.""DocEntry""=T1.""DocEntry""
                        INNER JOIN {_hanadb}.OBPL ""T2"" ON ""T0"".""BPLId"" = ""T2"".""BPLId""
                        WHERE T0.""CANCELED"" = 'N'
                            AND T0.""DocNum"" = {id}
                        ";
            return query;
        }
        internal string GetPurchaseRequestSpecificLines(string id)
        {
            string query =
                        $@"
                        SELECT 
                              T1.""ItemCode"" AS ""ItemCode""
                            , T1.""ItemCode"" || ' - ' ||  T1.""Dscription"" AS ""Dscription""
                            , CAST(T1.""Quantity"" AS DOUBLE) ""Quantity""
                            , T1.""OcrCode"" AS ""CostingCode""
                            , IFNULL(T2.""OcrCode"", '') || ' - ' || IFNULL(T2.""OcrName"", '') AS ""OcrName""
                        FROM ""{_hanadb}"".OPRQ T0
                            INNER JOIN ""{_hanadb}"".PRQ1 T1 ON T0.""DocEntry""=T1.""DocEntry""
                            LEFT JOIN ""{_hanadb}"".OOCR T2 ON T1.""OcrCode"" = T2.""OcrCode""
                        WHERE T0.""CANCELED"" = 'N'
                            AND T0.""DocNum"" = {id}
                        ";
            return query;
        }

        internal string GetItems()
        {
            string query = $@"SELECT 
                                  COALESCE(""ItemCode"", '') AS ""ItemCode""
                                , COALESCE(""ItemCode"", '') || ' - ' || COALESCE(""ItemName"", '') AS ""ItemName""
                            FROM ""{_hanadb}"".OITM
                            WHERE 
                                ""PrchseItem"" = 'Y'";
            return query;
        }

        internal string GetCostCenter()
        {
            string query = $@"SELECT 
	                              ""OcrCode"" ""CostingCode"" 
	                            , ""OcrCode"" || ' - ' || ""OcrName"" AS ""OcrName""
                            FROM ""{_hanadb}"".OOCR";

            return query;
        }

        internal string GetQueryDocEntry(string code)
        {
            string query = $@"SELECT ""DocEntry"" 
                                FROM ""{_hanadb}"".OPRQ T0
                                WHERE T0.""DocNum"" = {code} ";

            return query;
        }
    }
}