using System;
using ApprovalOneBff.Services.DTO;
using OneHana.Interfaces;

namespace ApprovalOneBff.Services.Services
{
    public class ApprovalQueries
    {
        private string _hanadb;
        private IOneHanaService _oneHanaService;

        public ApprovalQueries(string hanadb, IOneHanaService oneHanaService = null)
        {
            _hanadb = hanadb;
            _oneHanaService = oneHanaService;
        }

        public string GetQueryItemsByType(long recordId, string type)
        {
            string sql = "";

            switch (type)
            {
                case "SC":
                    sql = GetQueryItemsBySCOrPCOrDoc(recordId, 1470000113);
                    break;
                case "PC":
                    sql = GetQueryItemsBySCOrPCOrDoc(recordId, 22);
                    break;
                case "DOC":
                    sql = GetQueryItemsBySCOrPCOrDoc(recordId, 18);
                    break;
                case "CTT":
                    sql = GetQueryItemsByCTT(recordId);
                    break;
            }

            return sql;
        }

        public string GetQueryByType(long userId, string type, long substitute, string onlyPendings, long recordId = 0)
        {
            string sql = "";

            switch (type)
            {
                case "SC":
                    sql = GetQueryBySCOrPCOrDoc(userId, 1470000113, substitute, recordId, onlyPendings);
                    break;
                case "PC":
                    sql = GetQueryBySCOrPCOrDoc(userId, 22, substitute, recordId, onlyPendings);
                    break;
                case "DOC":
                    sql = GetQueryBySCOrPCOrDoc(userId, 18, substitute, recordId, onlyPendings);
                    break;
                case "CTT":
                    sql = GetQueryByCTT(userId, substitute, recordId, onlyPendings);
                    break;
            }

            return sql;
        }

        public string GetQueryTotalizers(long userId, long substitute, string onlyPendings)
        {
            string query = "";

            query = "SELECT COUNT(*) AS \"quantity\", 'SC' as \"type\" FROM ( " + GetQueryByType(userId, "SC", substitute, onlyPendings) + " ) ";
            query += " UNION ALL ";
            query += "SELECT COUNT(*) AS \"quantity\", 'PC' as \"type\" FROM ( " + GetQueryByType(userId, "PC", substitute, onlyPendings) + " ) ";
            query += " UNION ALL ";
            query += "SELECT COUNT(*) AS \"quantity\", 'CTT' as \"type\" FROM ( " + GetQueryByType(userId, "CTT", substitute, onlyPendings) + " ) ";
            query += " UNION ALL ";
            query += "SELECT COUNT(*) AS \"quantity\", 'DOC' as \"type\" FROM ( " + GetQueryByType(userId, "DOC", substitute, onlyPendings) + " ) ";

            return query;
        }

        private string GetQueryBySCOrPCOrDoc(long userId, long cardCode, long substitute, long recordId, string onlyPendings)
        {
            string query = "SELECT " +
                            "\"Approval\".\"WddCode\"                   AS \"id\", " +
                             "\"Owner\".\"U_NAME\"                      AS \"requester\", " +
                             "\"Approval\".\"DocDate\"                  AS \"requestDate\", " +
                             "\"ApprovalLine\".\"U_ALFA_DtApprove\"     AS \"approvalDate\", " +
                             "\"Approval\".\"Status\"                   AS \"status\", " +
                             "\"ApprovalLine\".\"U_ALFA_Status\"        AS \"statusApprover\", " +
                             "\"ApprovalLine\".\"U_ALFA_Remarks\"       AS \"approvalRemarks\", " +
                             "\"ApprovalLine\".\"U_ALFA_LineNum\"       AS \"lineNum\", " +
                             "\"ApprovalLine\".\"U_ALFA_Sequencia\"     AS \"sequence\", " +
                             "\"Approval\".\"Remarks\"                  AS \"remarks\", " +
                             "\"Draft\".\"DocTotal\"                    AS \"total\", " +
                             "\"Draft\".\"CardCode\" || ' - ' || \"Draft\".\"CardName\" AS \"partner\" " +
                            $"FROM { _hanadb }.OWDD \"Approval\" " +
                            $"INNER JOIN { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine\" " +
                                "ON \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" = \"Approval\".\"WddCode\" " +
                            $"INNER JOIN { _hanadb }.OUSR \"Owner\"" +
                                "ON \"Owner\".\"INTERNAL_K\" = \"Approval\".\"OwnerID\" " +
                            $"INNER JOIN { _hanadb }.OUSR \"Approver\" " +
                                "ON \"Approver\".\"INTERNAL_K\" = \"ApprovalLine\".\"U_ALFA_UserId\" " +
                            $"INNER JOIN { _hanadb }.ODRF \"Draft\" " +
                                $"ON \"Draft\".\"DocEntry\" = \"Approval\".\"DraftEntry\" " +
                          "WHERE " +
                          $"\"Approval\".\"ObjType\" = { cardCode } " +
                          $"AND \"Approver\".\"INTERNAL_K\" IN ( {userId}, {substitute} ) " + 
                          "AND \"ApprovalLine\".\"U_ALFA_StatusSequencia\" <> 'P' ";

            if (onlyPendings == "yes")
            {
                query += "AND \"Approval\".\"Status\" = 'W' ";
                query += "AND \"ApprovalLine\".\"U_ALFA_Status\" = 'W' ";
                query += "AND \"ApprovalLine\".\"U_ALFA_StatusSequencia\" = 'A' ";
            }

            if (recordId != 0)
            {
                query += $" AND \"Approval\".\"WddCode\" = { recordId } ";
            }

            query += "ORDER BY \"Approval\".\"WddCode\" DESC";

            return query;
        }

        internal string GetUpdatePendingToIgnorated(long id, int sequence = 0)
        {
            string update = $"UPDATE {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                            "SET " +
                            "\"U_ALFA_StatusSequencia\" = 'I', " +
                            "\"U_ALFA_Status\" = 'I' " +
                            $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                            "AND ( \"U_ALFA_StatusSequencia\" = 'A' OR \"U_ALFA_StatusSequencia\" = 'P')";

            if (sequence != 0)
            {
                update += $"AND \"U_ALFA_Sequencia\" = {sequence} ";
            }

            return update;
        }

        internal string GetQueryNextStagePending(long id, long sequence, string lineNum)
        {
            string sql = "SELECT TOP 1 \"U_ALFA_LineNum\" " +
                         $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" afa " +
                         $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                         $"AND \"U_ALFA_LineNum\" = {lineNum} " +
                         $"AND \"U_ALFA_Sequencia\" > {sequence} ";
            return sql;
        }

        internal string GetQueryDetailCurrentStage(long id, long sequence, string line)
        {
            string sql = "SELECT \"U_ALFA_LineNum\" AS \"Line\", " +
                                        "\"Approved\", " +
                                        "\"Rejected\", " +
                                        "\"Waiting\", " +
                                        "\"MaxApprov\", " +
                                        "\"MaxRej\" " +
                                    "FROM " +
                                    "(SELECT \"U_ALFA_LineNum\", " +
                                            "\"U_ALFA_Status\", " +
                                            "IFNULL( " +
                                                    "(SELECT COUNT(*) " +
                                                        $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" afa " +
                                                        $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                        $"AND \"U_ALFA_Sequencia\" = {sequence} " +
                                                        $"AND \"U_ALFA_LineNum\" = {line} " +
                                                        "AND \"U_ALFA_Status\" = 'Y' " +
                                                        "AND afa.\"U_ALFA_LineNum\" = afa3.\"U_ALFA_LineNum\"  " +
                                                        "GROUP BY \"U_ALFA_Status\"), 0) AS \"Approved\", " +
                                            "IFNULL( " +
                                                    "(SELECT COUNT(*) " +
                                                        $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" afa2 " +
                                                        $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                        $"AND \"U_ALFA_Sequencia\" = {sequence} " +
                                                        $"AND \"U_ALFA_LineNum\" = {line} " +
                                                        "AND \"U_ALFA_Status\" = 'N' " +
                                                        "AND afa2.\"U_ALFA_LineNum\" = afa3.\"U_ALFA_LineNum\"  " +
                                                        "GROUP BY \"U_ALFA_Status\"), 0) AS \"Rejected\", " +
                                            "IFNULL( " +
                                                    "(SELECT COUNT(*) " +
                                                        $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" afa4 " +
                                                        $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                        $"AND \"U_ALFA_Sequencia\" = {sequence} " +
                                                        $"AND \"U_ALFA_LineNum\" = {line} " +
                                                        "AND \"U_ALFA_Status\" = 'W' " +
                                                        "AND afa4.\"U_ALFA_LineNum\" = afa3.\"U_ALFA_LineNum\" " +
                                                        "GROUP BY \"U_ALFA_Status\"), 0) AS \"Waiting\", " +
                                            "\"MaxReqr\" AS \"MaxApprov\", " +
                                            "\"MaxRejReqr\" AS \"MaxRej\" " +
                                    $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" afa3 " +
                                    $"INNER JOIN {_hanadb}.\"OWST\" o ON o.\"WstCode\" = afa3.\"U_ALFA_Etapa\" " +
                                    $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                    $"AND \"U_ALFA_Sequencia\" = {sequence} " +
                                    $"AND \"U_ALFA_LineNum\" = {line} ) " +
                                    "GROUP BY \"U_ALFA_LineNum\", " +
                                            "\"Approved\", " +
                                            "\"Rejected\", " +
                                            "\"Waiting\", " +
                                            "\"MaxApprov\", " +
                                            "\"MaxRej\" ";

            return sql;
        }

        internal string GetQueryDetailApprove(long recordId, string type)
        {
            string objType = "";

            switch (type)
            {

                case "SC":
                    objType = "1470000113";
                    break;
                case "PC":
                    objType = "22";
                    break;
                case "DOC":
                    objType = "18";
                    break;
                case "CTT":
                    objType = "1250000027";
                    break;
            }

            string query = "SELECT \"ApprovalLine\".\"U_ALFA_Status\" as \"status\", " +
                            "\"ApprovalLine\".\"U_ALFA_DtApprove\" as \"date\", " +
                            "\"ApprovalLine\".\"U_ALFA_HrApprove\" as \"hour\", " +
                            "\"Approver\".\"U_NAME\" as \"name\", " +
                            "IFNULL(\"Approver2\".\"U_NAME\", '') as \"substitute\" " +
                             $"FROM { _hanadb }.OWDD \"Approval\" " +
                            $"INNER JOIN { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine\" " +
                                "ON \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" = \"Approval\".\"WddCode\" " +
                            $"INNER JOIN { _hanadb }.OUSR \"Approver\" " +
                            "ON \"Approver\".\"INTERNAL_K\" = \"ApprovalLine\".\"U_ALFA_UserId\" " +
                            $"LEFT JOIN { _hanadb }.OUSR \"Approver2\" " +
                            "ON \"Approver2\".\"INTERNAL_K\" = \"ApprovalLine\".\"U_ALFA_SubstId\" " +
                            "WHERE " +
                            $" \"Approval\".\"WddCode\" = { recordId } " +
                            $"AND \"Approval\".\"ObjType\" = { objType } " +
                            $"AND \"ApprovalLine\".\"U_ALFA_ObjType\"  = {objType} ";

            return query;
        }

        internal string GetQueryRecordByStatus(long id, string lineNum)
        {
            string query = "SELECT * " +
                            "FROM " +
                            "(SELECT 'W' AS \"status\", " +
                                    "COALESCE( " +
                                                "(SELECT COUNT(*) AS \"Total\" " +
                                                $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                                                $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                $"AND \"U_ALFA_LineNum\" = {lineNum} " +
                                                "AND \"U_ALFA_Status\" = 'W'), 0) AS \"total\" " +
                            $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                            "GROUP BY \"U_ALFA_Status\" " +

                            "UNION " +

                            "SELECT 'Y' AS \"status\", " +
                                        "COALESCE( " +
                                                    "(SELECT COUNT(*) AS \"TOTAL\" " +
                                                    $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                                                    $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                    $"AND \"U_ALFA_LineNum\" = {lineNum} " +
                                                    "AND \"U_ALFA_Status\" = 'Y'), 0) AS \"total\" " +
                            $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                            "GROUP BY \"U_ALFA_Status\" " +

                            "UNION " +

                            "SELECT 'N' AS \"status\", " +
                                            "COALESCE( " +
                                                        "(SELECT COUNT(*) AS \"TOTAL\" " +
                                                        $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                                                        $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                        $"AND \"U_ALFA_LineNum\" = {lineNum} " +
                                                        "AND \"U_ALFA_Status\" = 'N'), 0) AS \"total\" " +
                            $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                            "GROUP BY \"U_ALFA_Status\" " +

                            "UNION " +

                            "SELECT 'A' AS \"status\", " +
                                        "COALESCE( " +
                                                    "(SELECT COUNT(*) AS \"TOTAL\" " +
                                                    $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                                                    $"WHERE " +
                                                    $"\"U_ALFA_OWDDCodigo\" = {id} " +
                                                    $"AND \"U_ALFA_LineNum\" = {lineNum}), 0) AS \"total\" " +
                            $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                            "GROUP BY \"U_ALFA_Status\") TAB " +
                            "ORDER BY \"status\" ";

            return query;
        }

        private string GetQueryByCTT(long userId, long substitute, long recordId, string onlyPendings)
        {
            string query = "SELECT " +
                                "\"id\", \"requester\", \"requestDate\", \"approvalDate\"," +
                                "\"status\", \"statusApprover\", \"approvalRemarks\", " +
                                "\"lineNum\", \"sequence\", \"remarks\", \"partner\", " +
                                "SUM(\"total\") AS \"total\" " +
                            "FROM ( " +
                            "SELECT " +
                            "\"Approval\".\"WddCode\"                   AS \"id\", " +
                             "\"Owner\".\"U_NAME\"                      AS \"requester\", " +
                             "\"Approval\".\"DocDate\"                  AS \"requestDate\", " +
                             "\"ApprovalLine\".\"U_ALFA_DtApprove\"     AS \"approvalDate\", " +
                             "\"Approval\".\"Status\"                   AS \"status\", " +
                             "\"ApprovalLine\".\"U_ALFA_Status\"        AS \"statusApprover\", " +
                             "\"ApprovalLine\".\"U_ALFA_Remarks\"       AS \"approvalRemarks\", " +
                             "\"ApprovalLine\".\"U_ALFA_LineNum\"       AS \"lineNum\", " +
                             "\"ApprovalLine\".\"U_ALFA_Sequencia\"     AS \"sequence\", " +
                             "\"Approval\".\"Remarks\"                  AS \"remarks\", " +
                             "\"ContractDraft\".\"BpCode\" || ' - ' || \"ContractDraft\".\"BpName\" AS \"partner\", " +
                             "( Sum(\"ContractDraftItems\".\"UnitPrice\") * Sum(\"ContractDraftItems\".\"PlanQty\") ) AS \"total\" " +
                            $"FROM { _hanadb }.OWDD \"Approval\" " +
                            $"INNER JOIN { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine\" " +
                                "ON \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" = \"Approval\".\"WddCode\" " +
                            $"INNER JOIN { _hanadb }.OUSR \"Owner\"" +
                                "ON \"Owner\".\"INTERNAL_K\" = \"Approval\".\"OwnerID\" " +
                            $"INNER JOIN { _hanadb }.OUSR \"Approver\" " +
                                "ON \"Approver\".\"INTERNAL_K\" = \"ApprovalLine\".\"U_ALFA_UserId\" " +
                            $"INNER JOIN { _hanadb }.ooat \"ContractDraft\" " +
                                "ON \"ContractDraft\".\"AbsID\" = \"Approval\".\"DraftEntry\" " +
                            $"INNER JOIN { _hanadb }.oat1 \"ContractDraftItems\" " +
                                "ON \"ContractDraftItems\".\"AgrNo\" = \"ContractDraft\".\"AbsID\" " +
                          "WHERE " +
                            "\"Approval\".\"ObjType\" = 1250000027 " +
                            $"AND \"Approver\".\"INTERNAL_K\" IN ( {userId}, {substitute} ) " +
                            "AND \"ApprovalLine\".\"U_ALFA_StatusSequencia\" <> 'P' ";

            if (onlyPendings == "yes")
            {
                query += "AND \"Approval\".\"Status\" = 'W' ";
                query += "AND \"ApprovalLine\".\"U_ALFA_Status\" = 'W' ";
                query += "AND \"ApprovalLine\".\"U_ALFA_StatusSequencia\" = 'A' ";
            }

            if (recordId != 0)
            {
                query += $" AND \"Approval\".\"WddCode\" = { recordId } ";
            }

            query += "GROUP BY \"Approval\".\"WddCode\", \"Owner\".\"U_NAME\", " +
                    "\"Approval\".\"DocDate\", \"ApprovalLine\".\"U_ALFA_DtApprove\", " +
                    "\"ApprovalLine\".\"U_ALFA_Status\", \"ApprovalLine\".\"U_ALFA_Remarks\", " +
                    "\"ContractDraft\".\"BpCode\", \"ContractDraft\".\"BpName\", " +
                    "\"Approval\".\"Remarks\", \"Approval\".\"Status\", " +
                    "\"ApprovalLine\".\"U_ALFA_LineNum\", " +
                    "\"ApprovalLine\".\"U_ALFA_Sequencia\", " +
                    "\"ContractDraftItems\".\"AgrLineNum\", "+
                    "\"ContractDraftItems\".\"ItemCode\" " +
                    ") TAB " +
                    "GROUP BY \"id\", "+
                    "\"requester\", "+
                    "\"requestDate\", "+
                    "\"approvalDate\", "+
                    "\"status\", "+
                    "\"statusApprover\", "+
                    "\"approvalRemarks\", "+
                    "\"lineNum\", "+
                    "\"sequence\", "+
                    "\"remarks\", "+
                    "\"partner\" " + 
                    "ORDER BY \"id\" DESC";
            return query;
        }


        private string GetQueryItemsBySCOrPCOrDoc(long id, long type)
        {
            string query = "SELECT " +
              "\"DraftLine\".\"ItemCode\" || ' - ' || \"DraftLine\".\"Dscription\"   AS \"description\", " +
                "\"DraftLine\".\"Quantity\"                                          AS \"quantity\", " +
                "\"DraftLine\".\"Price\"                                             AS \"price\", " +
                "\"DraftLine\".\"LineTotal\"                                         AS \"total\", " +
                "\"DraftLine\".\"AcctCode\"                                          AS \"account\", " +
                "\"DraftLine\".\"LineNum\"                                           AS \"lineNum\", " +
                "\"CostCenter\".\"PrcCode\" || ' - ' || \"CostCenter\".\"PrcName\"   AS \"costCenter\", " +
                "IFNULL(\"Contract\".\"Number\", 0)                                  AS \"contractNumber\", " +
                "IFNULL((\"PlanQty\" - \"UndlvQty\"), 0)                             AS \"physicalBalance\", " +
                "IFNULL(((\"ContractItem\".\"PlanQty\" * \"ContractItem\".\"UnitPrice\") - \"ContractItem\".\"CumAmntLC\"), 0) AS \"financialBalance\" " +
                " FROM " +
                $"{_hanadb}.OWDD \"Approval\" " +
                $"INNER JOIN {_hanadb}.ODRF \"Draft\" " +
                " ON \"Draft\".\"DocEntry\" = \"Approval\".\"DraftEntry\" " +
                $"INNER JOIN {_hanadb}.DRF1 \"DraftLine\" " +
                " ON \"DraftLine\".\"DocEntry\" = \"Draft\".\"DocEntry\" " +
                $"LEFT JOIN {_hanadb}.OPRC \"CostCenter\" " +
                  "ON \"CostCenter\".\"PrcCode\" = \"DraftLine\".\"OcrCode\" " +
                $"LEFT JOIN {_hanadb}.OOAT \"Contract\" " +
                  "ON \"Contract\".\"AbsID\" = \"DraftLine\".\"AgrNo\" " +
                $"LEFT JOIN {_hanadb}.OAT1 \"ContractItem\" " +
                  "ON \"ContractItem\".\"AgrNo\" = \"DraftLine\".\"AgrNo\" AND \"ContractItem\".\"AgrLineNum\" = \"DraftLine\".\"AgrLnNum\" " +
                " WHERE " +
                $"\"Approval\".\"WddCode\" = '{id}' " +
                $" AND \"Approval\".\"ObjType\" = { type } ";

            return query;
        }

        internal string GetQueryItemsByCTT(long id)
        {
            string query = "SELECT " +
                "\"ContractDraftItems\".\"ItemCode\" || ' - ' || \"ContractDraftItems\".\"ItemName\"    AS \"description\", " +
                "\"ContractDraftItems\".\"PlanQty\"                                                     AS \"quantity\", " +
                "\"ContractDraftItems\".\"UnitPrice\"                                                   AS \"price\", " +
                "(\"ContractDraftItems\".\"PlanQty\" * \"ContractDraftItems\".\"UnitPrice\")            AS \"total\", " +
                "\"CostCenter\".\"PrcCode\" || ' - ' || \"CostCenter\".\"PrcName\"                      AS \"costCenter\" " +
                $"FROM { _hanadb }.OWDD \"Approval\" " +
                $"INNER JOIN { _hanadb }.ooat \"ContractDraft\" " +
                    "ON \"ContractDraft\".\"AbsID\" = \"Approval\".\"DraftEntry\" " +
                $"INNER JOIN { _hanadb }.oat1 \"ContractDraftItems\" " +
                    "ON \"ContractDraftItems\".\"AgrNo\" = \"ContractDraft\".\"AbsID\" " +
                $"LEFT JOIN {_hanadb}.OPRC \"CostCenter\" " +
                  "ON \"CostCenter\".\"PrcCode\" = \"ContractDraft\".\"U_ALFA_CentroCusto\" " +
                "WHERE " +
                    "\"Approval\".\"ObjType\" = 1250000027 " +
                    $" AND \"Approval\".\"WddCode\" = { id } ";

            return query;
        }

        public string GetQueryAttachments(long id, string type)
        {

            if (type == "CTT")
            {
                return GetQueryAttachmentsCTT(id);
            }
            else
            {
                return GetQueryAttachmentsSCOrPCOrDoc(id);
            }
        }

        internal string GetQueryAttachmentsSCOrPCOrDoc(long id)
        {
            string query = "SELECT " +
                            " \"AttachmentLine\".\"AbsEntry\" AS \"idApproval\", " +
                            " \"AttachmentLine\".\"Line\"     AS \"idLine\", " +
                            " \"AttachmentLine\".\"FileName\" || '.' || \"AttachmentLine\".\"FileExt\"      AS \"fileNamePublic\" " +
                            " FROM " +
                            $"{_hanadb}.OWDD \"Approval\" " +
                            $"INNER JOIN {_hanadb}.ODRF \"Draft\" " +
                            "ON \"Draft\".\"DocEntry\" = \"Approval\".\"DraftEntry\" " +
                            $"INNER JOIN {_hanadb}.ATC1 \"AttachmentLine\" " +
                            "ON \"Draft\".\"AtcEntry\" = \"AttachmentLine\".\"AbsEntry\" " +
                            " WHERE " +
                            $" \"Approval\".\"WddCode\" = '{id}' ";

            return query;
        }

        internal string GetQueryAttachmentsCTT(long id)
        {
            string query = "SELECT " +
                            "\"AttachmentLine\".\"AbsEntry\" AS \"idApproval\", " +
                            "\"AttachmentLine\".\"Line\" AS \"idLine\", " +
                            "\"AttachmentLine\".\"FileName\" || '.' || \"AttachmentLine\".\"FileExt\" AS \"fileNamePublic\", " +
                            "\"Approval\".\"WddCode\" " +
                            $"FROM {_hanadb}.OWDD \"Approval\" " +
                            $"INNER JOIN {_hanadb}.ooat \"ContractDraft\" " +
                            "ON \"ContractDraft\".\"AbsID\" = \"Approval\".\"DraftEntry\" " +
                            $"INNER JOIN {_hanadb}.ATC1 \"AttachmentLine\" " +
                            "ON \"ContractDraft\".\"AtchEntry\" = \"AttachmentLine\".\"AbsEntry\" " +
                            "WHERE " +
                            $"\"Approval\".\"WddCode\" = {id} ";

            return query;
        }

        internal string GetQueryAttachment(string attachmentId, string line)
        {
            string query = "SELECT " +
                    "\"AttachmentLine\".\"trgtPath\" || '\\' || \"AttachmentLine\".\"FileName\" || '.' || \"AttachmentLine\".\"FileExt\" AS \"fullpath\", " +
                    "\"AttachmentLine\".\"FileName\" || '.' || \"AttachmentLine\".\"FileExt\" AS \"filename\" " +
                    "FROM " +
                    $"{_hanadb}.ATC1 \"AttachmentLine\" " +
                    "WHERE " +
                    $"\"AttachmentLine\".\"AbsEntry\" = {attachmentId} " +
                    $"AND \"AttachmentLine\".\"Line\" = {line}";

            return query;
        }

        internal string GetQuerySubstituted(long userId)
        {
            string query = "SELECT \"Subst\".\"U_ALFA_UserAprov\" AS \"id\" " +
                           $"FROM {_hanadb}.\"@ALFA_SUBSTIT\" \"Subst\" " +
                           $"WHERE \"Subst\".\"U_ALFA_UserSubst\" = {userId} " +
                            "AND \"Subst\".\"U_ALFA_Active\" = 'Y' " +
                            $"AND TO_Date(CURRENT_DATE) >= To_Date(\"Subst\".\"U_ALFA_FromDate\") " +
                            $"AND TO_Date(CURRENT_DATE) <= To_Date(\"Subst\".\"U_ALFA_ToDate\") ";
            return query;
        }

        internal string GetUpdApproveOrReprove(long id, long substituted, ApprovalDTO data)
        {
            string status = data.Approved ? "Y" : "N";
            var now = GetNowHour();

            string update = $"UPDATE {_hanadb}.\"@ALFA_FLUXO_APROV\" SET " +
                            $"\"U_ALFA_Status\" = '{status}', " +
                            $"\"U_ALFA_DtApprove\" = CURRENT_DATE, " +
                            $"\"U_ALFA_Remarks\" = '{data.Remarks}', " +
                            $"\"U_ALFA_HrApprove\" = {now}, " +
                            "\"U_ALFA_StatusSequencia\" = 'F' ";

            if (substituted != 0)
            {
                update += $", \"U_ALFA_SubstId\" = {data.UserId} ";
            }

            update += $"WHERE \"U_ALFA_OWDDCodigo\" = {id} ";

            if (substituted != 0)
            {
                update += $"AND \"U_ALFA_UserId\" = {substituted} ";
            }
            else
            {
                update += $"AND \"U_ALFA_UserId\" = {data.UserId} ";
            }

            update += $"AND \"U_ALFA_LineNum\" = {data.LineNum} " +
                      $"AND \"U_ALFA_Sequencia\" = {data.Sequence} ";

            return update;
        }

        internal string GetUpdateReleaseNextSequence(long id, int sequence)
        {
            int nextSequence = sequence + 1;

            string update = "UPDATE " +
                      $"{ _hanadb }.\"@ALFA_FLUXO_APROV\" " +
                      "SET " +
                      "\"U_ALFA_StatusSequencia\" = 'A' " +
                      $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                      $"AND \"U_ALFA_Sequencia\" = {nextSequence}";
            
            return update;
        }

        private int GetNowHour()
        {
            var now = DateTime.Now;
            var hour = now.Hour.ToString();
            var minute = now.Minute.ToString();

            if (hour.Length == 1)
            {
                hour = "0" + hour;
            }

            if (minute.Length == 1)
            {
                minute = "0" + minute;
            }

            var hourWithMinute = hour + minute;

            return Int16.Parse(hourWithMinute);
        }
    }
}