namespace ApprovalOneBff.Services.Services
{
    public class ContractApprovalQueries
    {
        private string _hanadb;
        public ContractApprovalQueries(string hanadb)
        {
            _hanadb = hanadb;
        }

        public string GetPendent()
        {
            string query = "SELECT " +
                                "\"ApprovalHeader\".\"WddCode\" AS \"ID\", " +
                                "\"ApprovalHeader\".\"DocDate\" AS \"DocDate\", " +
                                "\"ApprovalLine\".\"UserID\" AS \"UserID\", " +
                                "\"Contract\".\"U_ALFA_CentroCusto\" AS \"CenterCost\", " +
                                "Sum(\"ContractItems\".\"UnitPrice\" * \"ContractItems\".\"PlanQty\" ) AS \"Total\", " +
                                "0 AS \"LineNum\", " +
                                "\"ApprovalHeader\".\"ObjType\" AS \"ObjType\" " +
                            $"FROM {_hanadb}.OWDD AS \"ApprovalHeader\" " +
                            $"INNER JOIN { _hanadb }.WDD1 AS \"ApprovalLine\" " +
                                "ON \"ApprovalLine\".\"WddCode\" = \"ApprovalHeader\".\"WddCode\" " +
                            $"INNER JOIN {_hanadb}.OOAT AS \"Contract\" " +
                                "ON \"Contract\".\"AbsID\" = \"ApprovalHeader\".\"DraftEntry\" " +
                            $"INNER JOIN { _hanadb }.OAT1 \"ContractItems\" " +
                                "ON \"ContractItems\".\"AgrNo\" = \"Contract\".\"AbsID\" " +
                            " WHERE (\"ApprovalHeader\".\"U_ALFA_FluxoAprv\" IS NULL OR \"ApprovalHeader\".\"U_ALFA_FluxoAprv\" = '' ) " +
                            " AND \"ApprovalHeader\".\"ObjType\" = 1250000027 " +
                            "GROUP BY    \"ApprovalHeader\".\"WddCode\", " +
                                        "\"ApprovalHeader\".\"DocDate\", " +
                                        "\"ApprovalLine\".\"UserID\", " +
                                        "\"Contract\".\"U_ALFA_CentroCusto\", " +
                                        "\"ApprovalHeader\".\"ObjType\" " +
                            "ORDER BY \"ApprovalHeader\".\"WddCode\" ";

            return query;
        }
    }
}