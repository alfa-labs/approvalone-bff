using System.Collections.Generic;
using ApprovalOneBff.Services.Models;

namespace ApprovalOneBff.Services.Services
{
    public class MarketingDocumentsApprovalQueries
    {
        private string _hanadb;
        public MarketingDocumentsApprovalQueries(string hanadb)
        {
            _hanadb = hanadb;
        }

        public string GetPendents()
        {
            string query = "SELECT " +
                "\"ApprovalHeader\".\"WddCode\"     AS \"ID\", " +
                "\"ApprovalHeader\".\"DocDate\"     AS \"DocDate\", " +
                "\"DraftLine\".\"OcrCode\"          AS \"CenterCost\", " +
                "\"DraftLine\".\"LineTotal\"        AS \"Total\", " +
                "\"DraftLine\".\"LineNum\"          AS \"LineNum\", " +
                "\"ApprovalHeader\".\"ObjType\"     AS \"ObjType\" " +
                " FROM " +
                $"{_hanadb}.OWDD \"ApprovalHeader\" " +
                $"INNER JOIN {_hanadb}.ODRF \"Draft\" " +
                " ON \"Draft\".\"DocEntry\" = \"ApprovalHeader\".\"DraftEntry\" " +
                $"INNER JOIN {_hanadb}.DRF1 \"DraftLine\" " +
                " ON \"DraftLine\".\"DocEntry\" = \"Draft\".\"DocEntry\" " +
                " WHERE (\"ApprovalHeader\".\"U_ALFA_FluxoAprv\" IS NULL OR \"ApprovalHeader\".\"U_ALFA_FluxoAprv\" = '' ) " +
                " AND \"ApprovalHeader\".\"ObjType\" IN (1470000113, 22, 18) ";

            return query;
        }
    }
}