using Microsoft.Extensions.Options;
using OneHanaUtil.Domain.Configuration;

namespace ApprovalOneBff.Services.Services
{
    public class MailQueries
    {
        private string _hanadb;
        private string _urlfront;
        public MailQueries(string hanadb, IOptions<Configuration> configurations)
        {
            _hanadb = hanadb;
            _urlfront = configurations.Value.ServiceLayer.UrlFront;
        }

        public string getMailPendent()
        {
            string query = $"SELECT *, '{_urlfront}/#/approvals/' || \"typeReg\" || '/' || \"id\" AS \"portalUrl\" FROM ( " + 
                "SELECT DISTINCT " +
                "\"Approval\".\"WddCode\"                                 AS \"id\", " +
                "\"Owner\".\"U_NAME\"                                         AS \"requester\", " +
                "\"Approval\".\"DocDate\"                                     AS \"requestDate\", " +
                "\"Draft\".\"DocTotal\"                                       AS \"total\", " +
                "\"Draft\".\"CardCode\" || ' - ' || \"Draft\".\"CardName\"    AS \"partner\", " +
                "\"Approver\".\"E_Mail\"                                      AS \"approverEmail\", " +
                "\"Approver\".\"U_NAME\"                                      AS \"approverName\", " +
                "\"ApprovalLine\".\"U_ALFA_UserId\"                           AS \"approverId\", " +
                "CASE " +
                    "WHEN \"Approval\".\"ObjType\" = 22 THEN 'PC' " +
                        "ELSE CASE " +
                            "WHEN \"Approval\".\"ObjType\" = 18 THEN 'DOC' " +
                            "ELSE 'SC' " +
                        "END " +
                "END AS \"typeReg\" " +
                "FROM " +
                $"{ _hanadb }.OWDD \"Approval\" " +
                $"INNER JOIN { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine\" " +
                "ON \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" = \"Approval\".\"WddCode\" " +
                $"INNER JOIN { _hanadb }.OUSR \"Owner\" " +
                "ON \"Owner\".\"INTERNAL_K\" = \"Approval\".\"OwnerID\" " +
                $"INNER JOIN { _hanadb }.OUSR \"Approver\" " +
                "ON \"Approver\".\"INTERNAL_K\" = \"ApprovalLine\".\"U_ALFA_UserId\" " +
                $"INNER JOIN { _hanadb }.ODRF \"Draft\" " +
                "ON \"Draft\".\"DocEntry\" = \"Approval\".\"DraftEntry\" " +
                "WHERE " +
                "\"Approval\".\"ObjType\" IN (22, 18, 1470000113) " +
                "AND \"ApprovalLine\".\"U_ALFA_Status\" = 'W' " +
                "AND ( ( \"ApprovalLine\".\"U_ALFA_StatusMail\" IS NULL ) " +
                " OR " +
                " ( \"ApprovalLine\".\"U_ALFA_StatusMail\" = 'S' AND  \"ApprovalLine\".\"U_ALFA_DtSendMail\" < TO_Date(CURRENT_DATE) ) ) " +
                "AND \"Approver\".\"E_Mail\" IS NOT NULL " +
                "AND \"U_ALFA_Sequencia\" = ( " +
                                    "SELECT MIN(\"U_ALFA_Sequencia\") " +
                                    $"FROM { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine3\" " +
                                    "WHERE \"ApprovalLine3\".\"U_ALFA_OWDDCodigo\" = \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" " +
                                    "AND ( (\"ApprovalLine3\".\"U_ALFA_StatusSequencia\" = 'P') OR (\"ApprovalLine3\".\"U_ALFA_StatusSequencia\" = 'A' AND  \"ApprovalLine3\".\"U_ALFA_DtSendMail\" < TO_Date(CURRENT_DATE) ) ) " +
                                ") " +
                "AND NOT EXISTS ( " +
                                    "SELECT \"ApprovalLine2\".\"U_ALFA_OWDDCodigo\" " +
                                    $"FROM { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine2\" " +
                                    "WHERE \"ApprovalLine2\".\"U_ALFA_OWDDCodigo\" = \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" " +
                                    "AND \"ApprovalLine2\".\"U_ALFA_StatusSequencia\" = 'A' " +
                                    "AND \"ApprovalLine2\".\"U_ALFA_DtSendMail\" = TO_Date(CURRENT_DATE) "+
                                ") " +
                "UNION ALL " +

                "SELECT " +
                "\"Approval\".\"WddCode\"           AS \"id\", " +
                "\"Owner\".\"U_NAME\"               AS \"requester\", " +
                "\"Approval\".\"DocDate\"           AS \"requestDate\", " +
                "( Sum(\"ContractDraftItems\".\"UnitPrice\") * Sum(\"ContractDraftItems\".\"PlanQty\") ) AS \"total\", " +
                "\"ContractDraft\".\"BpCode\" || ' - ' || \"ContractDraft\".\"BpName\" AS \"partner\", " +
                "\"Approver\".\"E_Mail\"                                    AS \"approverEmail\", " +
                "\"Approver\".\"U_NAME\"                                    AS \"approverName\", " +
                "\"ApprovalLine\".\"U_ALFA_UserId\"                         AS \"approverId\", " +
                $"'CTT'                                                     AS \"typeReg\" " +
                $"FROM { _hanadb }.OWDD \"Approval\" " +
                $"INNER JOIN { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine\" " +
                "ON \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" = \"Approval\".\"WddCode\" " +
                $"INNER JOIN { _hanadb }.OUSR \"Owner\"" +
                "ON \"Owner\".\"INTERNAL_K\" = \"Approval\".\"OwnerID\" " +
                $"INNER JOIN { _hanadb }.OUSR \"Approver\" " +
                "ON \"Approver\".\"INTERNAL_K\" = \"ApprovalLine\".\"U_ALFA_UserId\" " +
                $"INNER JOIN { _hanadb }.ooat \"ContractDraft\" " +
                "ON \"ContractDraft\".\"AbsID\" = \"Approval\".\"DraftEntry\" " +
                $"INNER JOIN { _hanadb }.oat1 \"ContractDraftItems\" " +
                "ON \"ContractDraftItems\".\"AgrNo\" = \"ContractDraft\".\"AbsID\" " +
                "WHERE " +
                "\"Approval\".\"ObjType\" = 1250000027 " +
                "AND \"ApprovalLine\".\"U_ALFA_Status\" = 'W' " +
                "AND ( ( \"ApprovalLine\".\"U_ALFA_StatusMail\" IS NULL ) " +
                " OR " +
                " ( \"ApprovalLine\".\"U_ALFA_StatusMail\" = 'S' AND  \"ApprovalLine\".\"U_ALFA_DtSendMail\" < TO_Date(CURRENT_DATE) ) ) " +
                "AND \"Approver\".\"E_Mail\" IS NOT NULL " +
                "AND \"U_ALFA_Sequencia\" = ( " +
                                    "SELECT MIN(\"U_ALFA_Sequencia\") " +
                                    $"FROM { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine3\" " +
                                    "WHERE \"ApprovalLine3\".\"U_ALFA_OWDDCodigo\" = \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" " +
                                    "AND ( (\"ApprovalLine3\".\"U_ALFA_StatusSequencia\" = 'P') OR (\"ApprovalLine3\".\"U_ALFA_StatusSequencia\" = 'A' AND  \"ApprovalLine3\".\"U_ALFA_DtSendMail\" < TO_Date(CURRENT_DATE) ) ) " +
                                ") " +
                "AND NOT EXISTS ( " +
                                    "SELECT DISTINCT \"ApprovalLine2\".\"U_ALFA_OWDDCodigo\" " +
                                    $"FROM { _hanadb }.\"@ALFA_FLUXO_APROV\" \"ApprovalLine2\" " +
                                    "WHERE \"ApprovalLine2\".\"U_ALFA_OWDDCodigo\" = \"ApprovalLine\".\"U_ALFA_OWDDCodigo\" " +
                                    "AND \"ApprovalLine2\".\"U_ALFA_StatusSequencia\" = 'A' " +
                                    "AND \"ApprovalLine2\".\"U_ALFA_DtSendMail\" = TO_Date(CURRENT_DATE) "+
                                ") " +
                "GROUP BY \"Approval\".\"WddCode\", " +
                    "\"Owner\".\"U_NAME\", " +
                    "\"Approver\".\"U_NAME\", " +
                    "\"Approval\".\"DocDate\", " +
                    "\"ContractDraft\".\"BpCode\", " + 
                    "\"ContractDraft\".\"BpName\", " +
                    "\"ApprovalLine\".\"U_ALFA_UserId\", " +
                    "\"Approver\".\"E_Mail\" " +
                    ") ";
            
            return query;
        }

        public string getUpdateMail(long id, long approverId) {
            string update = "UPDATE " + 
                  $"{ _hanadb }.\"@ALFA_FLUXO_APROV\" SET \"U_ALFA_StatusMail\" = 'S', " +
                  " \"U_ALFA_DtSendMail\" = CURRENT_DATE, " +
                  " \"U_ALFA_StatusSequencia\" = 'A' " +
                  $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " + 
                  $"AND \"U_ALFA_UserId\" = {approverId}";
                  
            return update;  
        }
    }
}