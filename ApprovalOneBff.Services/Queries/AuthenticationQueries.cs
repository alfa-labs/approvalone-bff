namespace ApprovalOneBff.Services.Services
{
    public class AuthenticationQueries
    {

        private string _hanadb;

        public AuthenticationQueries(string hanadb)
        {
            _hanadb = hanadb;
        }

        public string getQueryInfoUserLogin(string username)
        {
            string sql = @$"SELECT ""U_NAME"" as ""name"", ""INTERNAL_K"" as ""id"" FROM {_hanadb}.OUSR WHERE ""USER_CODE"" = '{username}'";
            return sql;
        }

    }
}