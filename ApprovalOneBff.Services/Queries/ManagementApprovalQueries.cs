using System.Collections.Generic;
using ApprovalOneBff.Services.Models;

namespace ApprovalOneBff.Services.Services
{
    public class ManagementApprovalQueries
    {
        private string _hanadb;
        
        public ManagementApprovalQueries(string hanadb)
        {
            _hanadb = hanadb;
        }
        
        internal string GetUsersWST1(long idEtapa)
        {
            string query = "SELECT \"UserID\" As \"Id\" " +
                            $"FROM {_hanadb}.WST1 " +
                            $"WHERE \"WstCode\" = {idEtapa} ";

            return query;
        }

        internal List<string> GetInsert(DocumentsApprovalFlow item, IEnumerable<UserApproval> users, Rule etapa)
        {
            var inserts = new List<string>();

            foreach (var user in users)
            {
                string userId = user.Id.ToString();
                string uniqueKey = $"{item.Id.ToString()}-{item.LineNum.ToString()}-{etapa.Etapa}-{userId}";

                string insert = $"INSERT INTO {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                                "(\"Code\", \"Name\", \"U_ALFA_UserId\",\"U_ALFA_Status\", " +
                                "\"U_ALFA_Etapa\", \"U_ALFA_OWDDCodigo\",  \"U_ALFA_ObjType\", " +
                                "\"U_ALFA_LineNum\", \"U_ALFA_Sequencia\", \"U_ALFA_StatusSequencia\") " +
                                "VALUES " +
                                $"('{uniqueKey}', '{uniqueKey}', '{userId}', " + 
                                $"'W', {etapa.Etapa}, {item.Id}, '{item.ObjType}', " +
                                $"{item.LineNum}, {etapa.Ordem}, 'P' ) ";

                inserts.Add(insert);
            }

            return inserts;
        }

        internal string GetQueryAllRulesByCC(string centerCost, decimal total, string docType)
        {
            string totalStr = total.ToString("F").Replace(',', '.');
            string query = "SELECT " +
                            "\"t2\".\"U_ALFA_NomeEtapa\" AS \"etapa\", " +
                            "\"t2\".\"U_ALFA_Ordem\" AS \"ordem\" " +
                            $"FROM {_hanadb}.\"@ALFA_AUTORIZACAO\" AS \"t1\" " +
                            $"INNER JOIN {_hanadb}.\"@ALFA_AUTORIZACAO1\" AS \"t2\" " +
                            "ON \"t1\".\"DocEntry\" = \"t2\".\"DocEntry\" " +
                            "WHERE " +
                            $" \"t1\".\"U_ALFA_CentroCusto\" = '{centerCost}' " +
                            $"AND \"t1\".\"U_ALFA_DocType\" = {docType} " +
                            $"AND {totalStr} >= \"t1\".\"U_ALFA_ValorInicial\" " +
                            $"AND {totalStr} <= \"t1\".\"U_ALFA_ValorFinal\" " + 
                            "ORDER BY \"t2\".\"U_ALFA_Ordem\" ";

            return query;
        }

        internal string getUpdateGenerated(long id)
        {
            string update = "UPDATE " +
                  $"{ _hanadb }.OWDD SET \"U_ALFA_FluxoAprv\" = 'S' " +
                  $"WHERE \"WddCode\" = {id} ";

            return update;
        }
        
        internal string GetQueryUsers(long id)
        {
            string query = "SELECT \"UserID\" As \"Id\" " +
                            $"FROM {_hanadb}.WST1 " +
                            $"WHERE \"WstCode\" = ( " +
                                                    "SELECT TOP 1 \"U_ALFA_Etapa\" as \"etapa\" " +
                                                    $"FROM {_hanadb}.\"@ALFA_FLUXO_APROV\" " +
                                                    $"WHERE \"U_ALFA_OWDDCodigo\" = {id} " +
                                                    ")";
            return query;
        }
    }
}