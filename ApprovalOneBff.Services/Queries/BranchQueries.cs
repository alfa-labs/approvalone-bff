namespace ApprovalOneBff.Services.Queries
{
  public class BranchQueries
  {
    private string _hanadb;

    public BranchQueries(string hanadb)
    {
      _hanadb = hanadb;
    }

    public string GetQueryBranchsByUser(string userCode) {
        string query = @$"SELECT ""TB1"".""BPLId"" ""branchId"", ""TB2"".""BPLName"" ""branchName""
                        FROM {_hanadb}.USR6 ""TB1""
                        INNER JOIN {_hanadb}.OBPL ""TB2""
                        ON ""TB1"".""BPLId"" = ""TB2"".""BPLId""
                        WHERE ""TB1"".""UserCode"" = '{userCode}' ";
        return query;
    }
  }
}