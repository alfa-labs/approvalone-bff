using System.Collections.Generic;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using ApprovalOneBff.Services.Queries;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Interface;

namespace ApprovalOneBff.Services.Services
{
  public class BranchService : IBranchService
  {

    private string HANA_DB;
    private BranchQueries _branchQueries;
    private IEasyServiceLayerService _serviceLayer;
    private IOneHanaService _oneHanaService;
    private IOptions<Configuration> _configurations;
    private ILoggerRepository _logger;

    public BranchService(IEasyServiceLayerService serviceLayer, IOneHanaService oneHanaService, IOptions<Configuration> configurations, ILoggerRepository logger)
    {
      _serviceLayer = serviceLayer;
      _oneHanaService = oneHanaService;
      _configurations = configurations;
      _logger = logger;

      HANA_DB = _configurations.Value.HanaDbConnection.Database;

      _branchQueries = new BranchQueries(HANA_DB);
    }

    public async Task<IEnumerable<Branch>> GetQueryBranchsByUser(string userCode)
    {
      string query = _branchQueries.GetQueryBranchsByUser(userCode);
      var result = await _oneHanaService.Query<Branch>(query);

      return result;
    }
  }
}