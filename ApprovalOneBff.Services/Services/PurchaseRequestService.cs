using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.EndPoints;
using OneServiceLayer.Interface;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace ApprovalOneBff.Services.Services
{
  public class PurchaseRequestService : IPurchaseRequest
  {
    private string HANA_DB;
    private PurchaseRequestQueries _PurchaseRequestQueries;
    private IEasyServiceLayerService _serviceLayer;
    private IOneHanaService _oneHanaService;
    private IOptions<Configuration> _configurations;
    private ILoggerRepository _logger;

    public PurchaseRequestService(IEasyServiceLayerService serviceLayer, IOneHanaService oneHanaService, IOptions<Configuration> configurations, ILoggerRepository logger)
    {
      _serviceLayer = serviceLayer;
      _oneHanaService = oneHanaService;
      _configurations = configurations;
      _logger = logger;

      HANA_DB = _configurations.Value.HanaDbConnection.Database;

      _PurchaseRequestQueries = new PurchaseRequestQueries(HANA_DB);
    }

    public async Task<IEnumerable<PurchaseRequest>> GetAllPurchaseRequest(string requester, string docNumber)
    {
      string query = _PurchaseRequestQueries.GetPurchaseRequestList(requester, docNumber);
      var resultPurchaseRequest = await _oneHanaService.Query<PurchaseRequest>(query);

      return resultPurchaseRequest;
    }

    public async Task<PurchaseRequest> GetSpecificPurchaseRequest(string id)
    {
      try
      {
        string query = _PurchaseRequestQueries.GetPurchaseRequestSpecific(id);
        var result = await _oneHanaService.QueryFirst<PurchaseRequest>(query);
        var queryLines = _PurchaseRequestQueries.GetPurchaseRequestSpecificLines(id);
        var resultLines = await _oneHanaService.Query<DocumentLines>(queryLines);
        result.DocumentLines = resultLines;

        return result;
      }
      catch (Exception e)
      {
        throw new Exception($"Erro ao incluir Log {e.Message}", e);
      }
    }

    public async Task<dynamic> PostPurchaseRequest(PurchaseRequest objPurchaseRequest)
    {
      var resultSL = await _serviceLayer.Post<dynamic>($"/{Endpoints.PurchaseRequests}",
                                                      objPurchaseRequest,
                                                      false,
                                                      0,
                                                      false);
      return resultSL.Data;
    }

    public async Task<dynamic> PatchPurchaseRequest(string id, PurchaseRequest objPurchaseRequest)
    {
      string query = _PurchaseRequestQueries.GetQueryDocEntry(id);
      var docEntry = await _oneHanaService.QueryFirst<DocEntryPurchaseRequest>(query);

      var result = await _serviceLayer.Patch<dynamic>($"/{Endpoints.PurchaseRequests}({docEntry.DocEntry})",
                                                      objPurchaseRequest,
                                                      false,
                                                      0,
                                                      true);

      return result;
    }

    public async Task<IEnumerable<Items>> GetItems()
    {

      string query = _PurchaseRequestQueries.GetItems();
      var result = await _oneHanaService.Query<Items>(query);

      return result;
    }

    public async Task<IEnumerable<CostCenter>> GetCostCenter()
    {

      string query = _PurchaseRequestQueries.GetCostCenter();
      var result = await _oneHanaService.Query<CostCenter>(query);

      return result;
    }

    public async Task<dynamic> CancelPurchaseRequest(string code)
    {
      string query = _PurchaseRequestQueries.GetQueryDocEntry(code);
      var docEntry = await _oneHanaService.QueryFirst<DocEntryPurchaseRequest>(query);

      var resultSL = await _serviceLayer.Post<dynamic>($"/{Endpoints.PurchaseRequests}({docEntry.DocEntry})/Cancel",
                                                      null,
                                                      false,
                                                      0,
                                                      false);
      return resultSL;
    }
  }
}