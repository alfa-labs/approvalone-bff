﻿using ApprovalOneBff.Services.DTO;
using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.EndPoints;
using OneServiceLayer.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Services
{
    public class ApprovalService : IApprovalService
    {
        private string HANA_DB;
        private ApprovalQueries _approvalQueries;
        private IEasyServiceLayerService _serviceLayer;
        private IOneHanaService _oneHanaService;
        private IOptions<Configuration> _configurations;
        private ILoggerRepository _logger;

        public ApprovalService(IEasyServiceLayerService serviceLayer, IOneHanaService oneHanaService, IOptions<Configuration> configurations, ILoggerRepository logger)
        {
            _serviceLayer = serviceLayer;
            _oneHanaService = oneHanaService;
            _configurations = configurations;
            _logger = logger;

            HANA_DB = _configurations.Value.HanaDbConnection.Database;

            _approvalQueries = new ApprovalQueries(HANA_DB, _oneHanaService);
        }

        public async Task<IEnumerable<Header>> GetApprovalsByUserId(long userId, string type, string onlyPendings, long recordId)
        {
            var substituted = await GetApprovalSubstituted(userId);
            string sql = _approvalQueries.GetQueryByType(userId, type, substituted, onlyPendings, recordId);

            var result = await _oneHanaService.Query<Header>(sql);

            return result;
        }

        public async Task<IEnumerable<Totalizers>> GetTotalizers(long id, string onlyPendings)
        {
            var substituted = await GetApprovalSubstituted(id);
            string query = _approvalQueries.GetQueryTotalizers(id, substituted, onlyPendings);

            var result = await _oneHanaService.Query<Totalizers>(query);

            return result;
        }

        public async Task<ResultApiServiceLayer> ApproveOrReprove(long id, ApprovalDTO data)
        {
            try
            {
                var substituted = await GetApprovalSubstituted(data.UserId);
                var update = _approvalQueries.GetUpdApproveOrReprove(id, substituted, data);
                await _oneHanaService.Execute(update);

                await ApprovaOrReproveContractOnSAPB1(id, data);

                return new ResultApiServiceLayer
                {
                    Status = true,
                    Data = null
                };
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "ApprovalService",
                    Method = "ApproveOrReprove",
                    Message = $"Erro ao aprovar/reprovar registro id {id}. {e.Message} "
                });

                return new ResultApiServiceLayer
                {
                    Status = false,
                    Data = e.Message
                };
            }
        }

        private async Task<dynamic> ExecuteApprovalOrRejectByServiceLayer(long id, ApprovalRequestDecisions decisions, bool isCtt)
        {
            var approvalRequests = new ApprovalRequests();
            approvalRequests.ApprovalRequestDecisions = new List<ApprovalRequestDecisions>();
            approvalRequests.ApprovalRequestDecisions.Add(decisions);

            var result = await _serviceLayer.Patch<dynamic>($"/{Endpoints.ApprovalRequests}({id})",
                                                            approvalRequests,
                                                            false,
                                                            0,
                                                            isCtt);

            return result;
        }

        private async Task ApprovaOrReproveContractOnSAPB1(long id, ApprovalDTO approval)
        {
            string status = await CheckApprovals(id, approval.Sequence, approval.LineNum);

            if (!String.IsNullOrEmpty(status))
            {
                var decisions = new ApprovalRequestDecisions
                {
                    Remarks = approval.Remarks,
                    Status = status,
                    ApproverUserName = _configurations.Value.ServiceLayer.ApprovalUsername,
                    ApproverPassword = _configurations.Value.ServiceLayer.ApprovalPassword
                };

                var resultSL = await ExecuteApprovalOrRejectByServiceLayer(id, decisions, true);
            }

        }

        private async Task<string> CheckApprovals(long id, int sequence, string lineNum)
        {
            var detailCurrentStage = await DetailCurrentStage(id, sequence, lineNum);
            var hasNextStagePending = await HasNextStagePending(id, sequence, lineNum);

            if (detailCurrentStage.Rejected >= detailCurrentStage.MaxRej)
            {
                ChangePendingToIgnorated(id);
                return "ardNotApproved";
            }

            if (detailCurrentStage.Approved >= detailCurrentStage.MaxApprov)
            {
                if (!hasNextStagePending)
                {
                    ChangePendingToIgnorated(id);
                    return "ardApproved";
                }
                else
                {
                    ChangePendingSequenceToIgnorated(id, sequence);
                    //ReleaseNextSequence(id, sequence);
                    return "";
                }
            }
            return "";
        }

        
        private void ReleaseNextSequence(long id, int sequence)
        {
            string update = _approvalQueries.GetUpdateReleaseNextSequence(id, sequence);
            _oneHanaService.Execute(update);
        }

        private void ChangePendingSequenceToIgnorated(long id, int sequence)
        {
            string sql = _approvalQueries.GetUpdatePendingToIgnorated(id, sequence);
            _oneHanaService.Execute(sql);
        }

        private void ChangePendingToIgnorated(long id)
        {
            string sql = _approvalQueries.GetUpdatePendingToIgnorated(id);
            _oneHanaService.Execute(sql);
        }

        private async Task<bool> HasNextStagePending(long id, long stage, string lineNum)
        {
            string query = _approvalQueries.GetQueryNextStagePending(id, stage, lineNum);
            var result = await _oneHanaService.QueryFirst<dynamic>(query);

            if (result != null)
                return true;

            return false;
        }

        private async Task<Confirmation> DetailCurrentStage(long id, int sequence, string lineNum)
        {
            string queryCurrentStage = _approvalQueries.GetQueryDetailCurrentStage(id, sequence, lineNum);
            var detailCurrentStage = await _oneHanaService.QueryFirst<Confirmation>(queryCurrentStage);

            return detailCurrentStage;
        }

        public async Task<ApprovalDetail> GetApprovalDetail(long userId, string type, long recordId = 0)
        {
            var substituted = await GetApprovalSubstituted(userId);
            string queryHeader = _approvalQueries.GetQueryByType(userId, type, substituted, "no", recordId);
            string queryItems = _approvalQueries.GetQueryItemsByType(recordId, type);
            string queryAttachment = _approvalQueries.GetQueryAttachments(recordId, type);
            string queryDetailApprove = _approvalQueries.GetQueryDetailApprove(recordId, type);

            var resultHeader = await _oneHanaService.QueryFirst<Header>(queryHeader);
            var resultItems = await _oneHanaService.Query<Item>(queryItems);
            var resultAttachment = await _oneHanaService.Query<Attachment>(queryAttachment);
            var resultDetailApprove = await _oneHanaService.Query<DetailApprove>(queryDetailApprove);

            var response = new ApprovalDetail
            {
                Header = resultHeader,
                Items = resultItems,
                Attachments = resultAttachment,
                DetailApprove = resultDetailApprove
            };

            return response;
        }

        public async Task<AttachmentInfo> GetAttachmentDetail(string attachmentId, string line)
        {
            string queryAttachment = _approvalQueries.GetQueryAttachment(attachmentId, line);

            var resultAttachment = await _oneHanaService.QueryFirst<AttachmentInfo>(queryAttachment);

            return resultAttachment;
        }

        private async Task<long> GetApprovalSubstituted(long userId)
        {
            string sqlSubstituted = _approvalQueries.GetQuerySubstituted(userId);

            var substituted = await _oneHanaService.QueryFirst<Substituted>(sqlSubstituted);

            if (substituted != null)
                return substituted.Id;
            else
                return 0;
        }
    }
}