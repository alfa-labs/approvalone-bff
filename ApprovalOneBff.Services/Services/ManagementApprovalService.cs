using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;

namespace ApprovalOneBff.Services.Services
{
    public class ManagementApprovalService : IManagementApprovalService
    {
        private string HANA_DB;
        private IOneHanaService _oneHanaService;
        private IOptions<Configuration> _configurations;
        private ManagementApprovalQueries _managementApprovalQueries;
        private ILoggerRepository _logger;

        public ManagementApprovalService(IOneHanaService oneHanaService, IOptions<Configuration> configurations, ILoggerRepository logger)
        {
            _oneHanaService = oneHanaService;
            _configurations = configurations;
            _logger = logger;

            HANA_DB = _configurations.Value.HanaDbConnection.Database;
            _managementApprovalQueries = new ManagementApprovalQueries(HANA_DB);
        }

        public async Task<IEnumerable<Rule>> GetEtapa(DocumentsApprovalFlow item)
        {
            string queryRuleByCC = _managementApprovalQueries.GetQueryAllRulesByCC(item.CenterCost, item.Total, item.ObjType);
            var rules = await _oneHanaService.Query<Rule>(queryRuleByCC);
            var hasRules = false;

            foreach (var tst in rules)
            {
                hasRules = true;
                break;
            }

            return hasRules ? rules : null;
        }

        public async Task<bool> Generate(DocumentsApprovalFlow item, IEnumerable<Rule> etapas)
        {
            try
            {
                foreach (var etapa in etapas)
                {
                    var users = await GetUsers(etapa.Etapa);
                    var inserts = _managementApprovalQueries.GetInsert(item, users, etapa);

                    foreach (var insert in inserts)
                    {
                        await _oneHanaService.Execute(insert);
                    }
                }
                updateGenerated(item.Id);
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "ApprovalContractService",
                    Method = "Generate",
                    Message = $"Erro ao atualizar a tabela de fluxo de contratos. {e.Message} "
                });

                throw new Exception("Erro ao atualizar a tabela de fluxo de contratos", e);
            }

            return true;
        }

        private void updateGenerated(long id)
        {
            string update = _managementApprovalQueries.getUpdateGenerated(id);
            _oneHanaService.Execute(update);
        }

        private async Task<IEnumerable<UserApproval>> GetUsers(long id)
        {
            string queryUser = _managementApprovalQueries.GetUsersWST1(id);
            var users = await _oneHanaService.Query<UserApproval>(queryUser);
            return users;
        }
    }
}