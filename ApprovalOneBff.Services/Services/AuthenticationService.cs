﻿using ApprovalOneBff.Services.DTO;
using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.EndPoints;
using OneServiceLayer.Interface;
using OneServiceLayer.Utilities;
using OneServiceLayer.ViewModels;
using System.Net;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private IEasyServiceLayerService _serviceLayer;
        private IOneHanaService _oneHanaService;
        private IOptions<Configuration> _configurations;
        private readonly string HANA_DB;
        private readonly AuthenticationQueries _authenticationQueries;

        public AuthenticationService(IEasyServiceLayerService serviceLayer, IOneHanaService oneHanaService, IOptions<Configuration> configurations)
        {
            _oneHanaService = oneHanaService;
            _serviceLayer = serviceLayer;
            _configurations = configurations;
            
            HANA_DB = _configurations.Value.HanaDbConnection.Database;

            _authenticationQueries = new AuthenticationQueries(HANA_DB);
        }

        public async Task<ResultViewModel> Login(LoginDTO login)
        {
            try
            {
                var loginResult = await _serviceLayer.Post<LoginResponse>($"/{Endpoints.Login}", login);

                if (loginResult.StatusCode != HttpStatusCode.OK)
                    return Responses.UnauthorizedErrorMessage();

                string username = login.UserName;
                User userResult = await getInfoUser(username);

                _configurations.Value.ServiceLayer.Username = login.UserName;
                _configurations.Value.ServiceLayer.Password = login.Password;
                _configurations.Value.ServiceLayer.SessionId = loginResult.Data.SessionId;

                return new ResultViewModel
                {
                    Message = "Sucesso",
                    Success = true,
                    Data = new
                    {
                        User = userResult,
                        sessionId = loginResult.Data.SessionId
                    }
                };
            }
            catch
            {
                return Responses.ApplicationErrorMessage();
            }
        }

        private async Task<User> getInfoUser(string username)
        {
            string sql = _authenticationQueries.getQueryInfoUserLogin(username);
            var userResult = await _oneHanaService.QueryFirst<User>(sql);

            return userResult;
        }


    }
}
