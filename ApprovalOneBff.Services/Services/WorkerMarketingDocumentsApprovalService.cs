using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Services
{
    public class WorkerMarketingDocumentsApprovalService : IWorkerMarketingDocumentsApprovalService
    {
        private string HANA_DB;
        private MarketingDocumentsApprovalQueries _marketingDocumentsQueries;
        private IOneHanaService _oneHanaService;
        private IOptions<Configuration> _configurations;

        private ILoggerRepository _logger;

        public WorkerMarketingDocumentsApprovalService(IOneHanaService oneHanaService, IOptions<Configuration> configurations, ILoggerRepository logger)
        {
            _oneHanaService = oneHanaService;
            _configurations = configurations;
            _logger = logger;

            HANA_DB = _configurations.Value.HanaDbConnection.Database;

            _marketingDocumentsQueries = new MarketingDocumentsApprovalQueries(HANA_DB);
        }

        public async Task<IEnumerable<DocumentsApprovalFlow>> GetDocumentsPendent()
        {
            try
            {
                string query = _marketingDocumentsQueries.GetPendents();
                var pendent = await _oneHanaService.Query<DocumentsApprovalFlow>(query);

                return pendent;
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "WorkerMarketingDocumentsApprovalService",
                    Method = "GetDocumentsPendent",
                    Message = $"Erro ao consultar documentos pendentes {e.Message} "
                });

                throw new Exception("Erro ao consultar documentos pendentes", e);
            }
        }
    }
}