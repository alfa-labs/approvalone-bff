using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.EndPoints;
using OneServiceLayer.Interface;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ApprovalOneBff.Services.Services
{
    public class StructureService : IStructureService
    {
        private IEasyServiceLayerService _serviceLayer;
        private IOptions<Configuration> _configurations;

        public StructureService(IEasyServiceLayerService serviceLayer, IOptions<Configuration> configurations)
        {
            _serviceLayer = serviceLayer;
            _configurations = configurations;
        }

        public async Task<dynamic> Structure()
        {
            string jsonPath = @"assets/structure.json";

            using (StreamReader r = new StreamReader(jsonPath))
            {
                string jsonString = r.ReadToEnd();
                var structure = JsonConvert.DeserializeObject<Structure>(jsonString);

                await CreateTables(structure.UserTables);
                await CreateFields(structure.UserFields);
                await CreateObjects(structure.UserObjects);
            }
            return true;
        }

        private async Task<bool> CreateTables(List<UserTable> userTables)
        {
            foreach (var item in userTables)
            {
                string tableName = $"TableName='{item.TableName}'";
                var result = await _serviceLayer.Get<dynamic>($"/{Endpoints.UserTablesMD}({tableName})", false, 0, true);
                if (result.StatusCode == HttpStatusCode.NotFound)
                {
                    await CreateTable(item);
                }
            }

            return true;
        }

        private async Task<bool> CreateFields(List<UserField> userFields)
        {
            foreach (var item in userFields)
            {
                string filter = $"$filter=TableName eq '{item.TableName}' and Name eq '{item.Name}'";
                var result = await _serviceLayer.Get<ODataSAPResponse>($"/{Endpoints.UserFieldsMD}?{filter}", false, 0, true);
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    if (result.Data.value.Count == 0)
                    {
                        await CreateField(item);
                    }
                }
            }
            return true;
        }

        private async Task<dynamic> CreateTable(UserTable item)
        {
            try
            {
                return await _serviceLayer.Post<dynamic>($"/{Endpoints.UserTablesMD}", item, false, 0, true);
            }
            catch (Exception e)
            {
                throw new Exception($"Erro ao criar a tabela ({item.TableName}) - {e.Message}", e);
            }
        }

        private async Task<dynamic> CreateField(UserField item)
        {
            try
            {
                return await _serviceLayer.Post<dynamic>($"/{Endpoints.UserFieldsMD}", item, false, 0, true);
            }
            catch (Exception e)
            {
                throw new Exception($"Erro ao criar o campo: {item.Name}", e);
            }
        }

        private async Task<bool> CreateObjects(List<dynamic> objects)
        {
            foreach (var item in objects)
            {
                try
                {
                    string identifier = $"'{item.Code}'";
                    var result = await _serviceLayer.Get<dynamic>($"/{Endpoints.UserObjectsMD}({identifier})", false, 0, true);
                    if (result.StatusCode == HttpStatusCode.NotFound)
                    {
                        await CreateObject(item);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Erro ao criar o UDO: {item.Name}", e);
                }
            }
            return true;
        }

        private async Task<dynamic> CreateObject(dynamic item)
        {
            try
            {
                return await _serviceLayer.Post<dynamic>($"/{Endpoints.UserObjectsMD}", item, false, 0, true);
            }
            catch (Exception e)
            {
                throw new Exception($"Erro ao criar o UDO: {item.Name}", e);
            }
        }
    }
}
