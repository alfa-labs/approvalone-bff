using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Coravel.Mailer.Mail.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;

namespace ApprovalOneBff.Services.Services
{
    public class MailService : IMailService
    {
        private string HANA_DB;
        private IOneHanaService _oneHanaService;
        private IOptions<Configuration> _configurations;
        private MailQueries _mailQueries;
        private IMailer _mailer;
        private ILoggerRepository _logger;

        private readonly IConfiguration _settings;

        public MailService( IOneHanaService oneHanaService,
                            IOptions<Configuration> configurations,
                            IConfiguration settings,
                            IMailer mailer,
                            ILoggerRepository logger)
        {
            _oneHanaService = oneHanaService;
            _configurations = configurations;
            _mailer = mailer;
            _logger = logger;
            _settings = settings;

            HANA_DB = _configurations.Value.HanaDbConnection.Database;
            _mailQueries = new MailQueries(HANA_DB, _configurations);
        }

        public Task<IEnumerable<Mail>> GetPendentMail()
        {
            var query = _mailQueries.getMailPendent();
            var result = _oneHanaService.Query<Mail>(query);

            return result;
        }

        public async Task<bool> SendMail(Mail mail)
        {
            string from = _settings.GetSection("Coravel:Mail:Username").Value;
            try
            {
                await _mailer.SendAsync(new PortalAprovacoesMailable(
                                                            new NewUserMailableViewModel
                                                            {
                                                                Email = mail.ApproverEmail,
                                                                CallbackUrl = mail.PortalUrl,
                                                                Id = mail.Id,
                                                                TypeReg = mail.TypeReg
                                                            },
                                                            from
                                                        ));

                updateSendMail(mail);
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "MailService",
                    Method = "SendMail",
                    Message = $"Erro ao enviar e-mail. {e.Message.Replace("Views", "Vi_ews").Replace("~", "")} "
                });

                throw new Exception("Erro ao enviar e-mail", e);
            }
            return true;
        }

        private async void updateSendMail(Mail mail)
        {
            try
            {
                string update = _mailQueries.getUpdateMail(mail.Id, mail.ApproverId);
                var result = _oneHanaService.Execute(update);
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "MailService",
                    Method = "updateSendMail",
                    Message = $"Erro ao atualizar o status de e-mail enviado! {e.Message} "
                });

                throw new Exception("Erro ao atualizar o status de e-mail enviado!", e);
            }
        }
    }
}