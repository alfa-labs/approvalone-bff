﻿using ApprovalOneBff.Services.Interfaces;
using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Services
{
    public class WorkerApprovalContractService : IWorkerApprovalContractService
    {
        private string HANA_DB;
        private ContractApprovalQueries _contractApprovalQueries;
        private IOneHanaService _oneHanaService;
        private IOptions<Configuration> _configurations;

        private ILoggerRepository _logger;

        public WorkerApprovalContractService(IOneHanaService oneHanaService, IOptions<Configuration> configurations, ILoggerRepository logger)
        {
            _oneHanaService = oneHanaService;
            _configurations = configurations;
            _logger = logger;

            HANA_DB = _configurations.Value.HanaDbConnection.Database;

            _contractApprovalQueries = new ContractApprovalQueries(HANA_DB);
        }

        public async Task<IEnumerable<DocumentsApprovalFlow>> GetContractPendent()
        {
            try
            {
                string query = _contractApprovalQueries.GetPendent();
                var pendent = await _oneHanaService.Query<DocumentsApprovalFlow>(query);

                return pendent;
            }
            catch (Exception e)
            {
                await _logger.Logger(new LogB1
                {
                    Owner = "ApprovalContractService",
                    Method = "GetContractPendent",
                    Message = $"Erro ao consultar contratos pendentes {e.Message} "
                });

                throw new Exception("Erro ao consultar contratos pendentes", e);
            }
        }
    }
}