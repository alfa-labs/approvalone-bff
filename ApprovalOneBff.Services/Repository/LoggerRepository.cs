﻿using ApprovalOneBff.Services.Models;
using Microsoft.Extensions.Options;
using OneHana.Interfaces;
using OneHanaUtil.Domain.Configuration;
using OneServiceLayer.Domain.LogApproval;
using OneServiceLayer.Interface;
using System;
using System.Threading.Tasks;

namespace OneServiceLayer.Repository
{
    public class LoggerRepository : ILoggerRepository
    {
        private readonly IOptions<Configuration> _configuration;
        private readonly IEasyServiceLayerService _serviceLayer;
        private readonly IOneHanaService _oneHana;
        private string HANA_DB;

        public LoggerRepository(IOptions<Configuration> configuration, IEasyServiceLayerService serviceLayer, IOneHanaService oneHana)
        {
            _configuration = configuration;
            _serviceLayer = serviceLayer;
            _oneHana = oneHana;

            HANA_DB = _configuration.Value.HanaDbConnection.Database;
        }

        public async Task Logger(LogB1 logb1)
        {
            try
            {
                await DeleteLogs();

                LogSAPB1Aprovacao log = new LogSAPB1Aprovacao
                {
                    LogTypeCode = 1,
                    Module = 1,
                    Message = logb1.Message,
                    FullMessage = logb1.Message,
                    Company = HANA_DB,
                    Key = "",
                    RequestObject = logb1.Request,
                    ResponseObject = "",
                    Database = HANA_DB,
                };

                var sql = $"INSERT INTO \"ALFA_LOGS\".\"LOG_PORTALCOMPRAS\" ( " +
                "\"LOGDATE\", " +
                "\"LOGHOUR\", " +
                "\"LOGTYPECODE\", " +
                "\"COMPANY\", " +
                "\"MODULE\", " +
                "\"MESSAGE\", " +
                "\"FULLMESSAGE\", " +
                "\"KEY\", " +
                "\"REQUESTOBJECT\", " +
                "\"RESPONSEOBJECT\", " +
                "\"OWNER\", " +
                "\"METHOD\" " +
            ") VALUES ( " +
                    $"CURRENT_DATE, " +
                    $"CURRENT_TIME, " +
                    $"{log.LogTypeCode}, " +
                    $"'{log.Company}', " +
                    $"{log.Module}, " +
                    $"'{log.Message}', " +
                    $"'{log.FullMessage}', " +
                    $"'{log.Key}', " +
                    $"'{log.RequestObject}', " +
                    $"'{log.ResponseObject}', " +
                    $"'{logb1.Owner}', " +
                    $"'{logb1.Method}' " +
                ") ";

                var result = await _oneHana.Execute(sql);

            }
            catch (Exception e)
            {
                throw new Exception($"Erro ao incluir Log {e.Message}", e);
            }
        }

        private async Task DeleteLogs()
        {
            string sql = @"DELETE FROM ""ALFA_LOGS"".""LOG_PORTALCOMPRAS"" WHERE LOGDATE < ADD_DAYS(CURRENT_DATE, -15)";
            await _oneHana.Execute(sql);
        }
    }
}