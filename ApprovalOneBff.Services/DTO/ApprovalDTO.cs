namespace ApprovalOneBff.Services.DTO
{
    public class ApprovalDTO
    {
        public long Id { get; set; }
        public bool Approved { get; set; }
        public string Remarks { get; set; }
        public string Type { get; set; }
        public string LineNum { get; set; }
        public long UserId { get; set; }
        public int Sequence { get; set; }

        public ApprovalDTO()
        { }

        public ApprovalDTO(long id, bool approved, string remarks, string type, long userId, string lineNum, int sequence)
        {
            Id = id;
            Approved = approved;
            Remarks = remarks;
            Type = type;
            UserId = userId;
            LineNum = lineNum;
            Sequence = sequence;
        }
    }
}