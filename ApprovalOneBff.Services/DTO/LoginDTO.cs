namespace ApprovalOneBff.Services.DTO {
    public class LoginDTO {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CompanyDB { get; set; }

        public LoginDTO()
        {}

        public LoginDTO(string username, string password, string companydb)
        {
            UserName = username;
            Password = password;
            CompanyDB = companydb;
        }
    }
}