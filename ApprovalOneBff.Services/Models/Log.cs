using System;

namespace ApprovalOneBff.Services.Models
{
    public class LogSAPB1Aprovacao
    {
        public int LogTypeCode { get; set; }
        public int Module { get; set; }
        public string Company { get; set; }
        public string Message { get; set; }
        public string FullMessage { get; set; }
        public string Key { get; set; }
        public string RequestObject { get; set; }
        public string ResponseObject { get; set; }
        public string Database { get; set; }

    }
}