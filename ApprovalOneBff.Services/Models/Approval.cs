﻿using System;
using System.Collections.Generic;

namespace ApprovalOneBff.Services.Models
{
    public class ApprovalDetail
    {
        public Header Header { get; set; }
        public IEnumerable<Item> Items { get; set; }
        public IEnumerable<Attachment> Attachments { get; set; }

        public IEnumerable<DetailApprove> DetailApprove { get; set; }
    }

    public class Header
    {
        private decimal _decimal { get; set; }
        private string _requestDate { get; set; }
        private string _approvalDate { get; set; }

        public int Id { get; set; }
        public string Requester { get; set; }
        public string Partner { get; set; }
        public string LineNum { get; set; }
        public string RequestDate
        {
            get
            {
                return String.IsNullOrEmpty(_requestDate) ? "" :
                            $"{_requestDate.Substring(3, 2)}" +
                            $"/{_requestDate.Substring(0, 2)}" +
                            $"/{_requestDate.Substring(6, 4)}";
            }
            set => _requestDate = value;
        }
        public string ApprovalDate
        {
            get
            {
                return String.IsNullOrEmpty(_approvalDate) ? "" :
                            $"{_approvalDate.Substring(3, 2)}" +
                            $"/{_approvalDate.Substring(0, 2)}" +
                            $"/{_approvalDate.Substring(6, 4)}";
            }
            set => _approvalDate = value;
        }
        public string Status { get; set; }
        public string StatusApprover { get; set; }
        public string ApprovalRemarks { get; set; }
        public string Remarks { get; set; }
        public long Sequence { get; set; }
        public dynamic Total
        {
            get => _decimal;
            set
            {
                _decimal = Convert.ToDecimal(value);
            }
        }
    }

    public class Item
    {
        private decimal _quantity { get; set; }
        private decimal _price { get; set; }
        private decimal _total { get; set; }
        private decimal _physicalBalance { get; set; }
        private decimal _financialBalance { get; set; }

        public string Description { get; set; }
        public string Account { get; set; }
        public string CostCenter { get; set; }
        public string LineNum { get; set; }
        public dynamic Quantity
        {
            get => _quantity;
            set
            {
                _quantity = Convert.ToDecimal(value);
            }
        }
        public dynamic Price
        {
            get => _price;
            set
            {
                _price = Convert.ToDecimal(value);
            }
        }
        public dynamic Total
        {
            get => _total;
            set
            {
                _total = Convert.ToDecimal(value);
            }
        }

        public long ContractNumber { get; set; }
        public dynamic PhysicalBalance
        {
            get => _physicalBalance;
            set
            {
                _physicalBalance = Convert.ToDecimal(value);
            }
        }

        public dynamic FinancialBalance
        {
            get => _financialBalance;
            set
            {
                _financialBalance = Convert.ToDecimal(value);
            }
        }
    }

    public class Attachment
    {
        public string IdApproval { get; set; }
        public string IdLine { get; set; }
        public string FileNamePublic { get; set; }
    }

    public class Totalizers
    {
        public string Type { get; set; }
        public long Quantity { get; set; }
    }

    public class ApprovalRequests
    {
        public List<ApprovalRequestDecisions> ApprovalRequestDecisions { get; set; }
    }

    public class ApprovalRequestDecisions
    {
        public string Status { get; set; }
        public string ApproverUserName { get; set; }
        public string ApproverPassword { get; set; }
        public string Remarks { get; set; }
    }

    public class ResultApiServiceLayer
    {
        public bool Status { get; set; }
        public dynamic Data { get; set; }
    }

    public class AttachmentInfo
    {
        public string fullpath;
        public string filename;
    }
}
