﻿namespace ApprovalOneBff.Services.Models
{
    public class User
    {
        public long id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }

    public class UserApproval {
        public long Id { get; set; }
    }
}
