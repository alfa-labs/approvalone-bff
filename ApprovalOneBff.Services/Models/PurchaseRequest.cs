using System.Collections.Generic;
using ApprovalOneBff.Services.Models;
using System;

namespace ApprovalOneBff.Services.Models
{
    public class PurchaseRequest
    {
        public int? DocNum { get; set; }
        public int? DocEntry { get; set; }
        public string? Requester { get; set; }
        public string? ReqName { get; set; }
        public int? BPL_IDAssignedToInvoice { get; set; }
        public string? DocStatus { get; set; }
        public string? Comments { get; set; }
        public string? RequriedDate { get; set; }
        public string? DocDate { get; set; }
        public string? TaxDate { get; set; }
        public string? DocDueDate { get; set; }
        public string? ReqDate { get; set; }
        public string? BranchName { get; set; }
        public IEnumerable<DocumentLines> DocumentLines { get; set; }

        public PurchaseRequest(int DocNum, int DocEntry, string Requester, string ReqName, int BPLId, string DocStatus, string Comments, string RequriedDate, string DocDate, string TaxDate, string DocDueDate, IEnumerable<DocumentLines> DocumentLines)
        {
            this.DocNum = DocNum;
            this.DocEntry = DocEntry;
            this.Requester = Requester;
            this.ReqName = ReqName;
            this.BPL_IDAssignedToInvoice = BPLId;
            this.DocStatus = DocStatus;
            this.Comments = Comments;
            this.RequriedDate = RequriedDate;
            this.DocDate = DocDate;
            this.TaxDate = TaxDate;
            this.DocDueDate = DocDueDate;
            this.DocumentLines = DocumentLines;
        }
        public PurchaseRequest(IEnumerable<DocumentLines> DocumentLines)
        {
            this.DocumentLines = DocumentLines;
        }
        public PurchaseRequest() { }

    }
    public class DocumentLines
    {
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public string OcrName { get; set; }
        public double Price { get; set; }
        public double Quantity { get; set; }
        public string CostingCode { get; set; }

        public DocumentLines(dynamic teste)
        {

        }
        public DocumentLines() { }
        public DocumentLines(string ItemCode, string Dscription, double Price, double Quantity)
        {
            this.ItemCode = ItemCode;
            this.Dscription = Dscription;
            this.Price = Price;
            this.Quantity = Quantity;
        }
    }

    public class DocEntryPurchaseRequest
    {
        public int DocEntry { get; set; }
    }
}