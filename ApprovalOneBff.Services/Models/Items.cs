﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Models
{
    public class Items
    {
        public string ItemName { get; set; }
        public string ItemCode { get; set; }

        public Items()
        {

        }
        public Items(string ItemName, string ItemCode)
        {
            this.ItemCode = ItemCode;
            this.ItemName = ItemName;
        }

    }
}
