using System;

namespace ApprovalOneBff.Services.Models
{
    public class DocumentsApprovalFlow
    {
        private decimal _total { get; set; }
        
        public long Id { get; set; }
        public int LineNum { get; set; }
        public DateTime DocDate { get; set; }
        public string ObjType { get; set; }
        public string CenterCost { get; set; }
        public dynamic Total
        {
            get => _total;
            set
            {
                _total = Convert.ToDecimal(value);
            }
        }
    }
}