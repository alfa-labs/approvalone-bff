namespace ApprovalOneBff.Services.Models
{
    public class Rule
    {
        public long Etapa { get; set; }
        public int Ordem { get; set; }
    }

    public class Confirmation
    {
        public string Line { get; set; }
        public int Approved { get; set; }
        public int Rejected { get; set; }
        public int Waiting { get; set; }
        public int MaxApprov { get; set; }
        public int MaxRej { get; set; }
    }
}