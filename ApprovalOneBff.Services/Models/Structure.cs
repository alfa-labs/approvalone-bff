using System.Collections.Generic;

namespace ApprovalOneBff.Services.Models
{
    public class Structure
    {
        public List<UserTable> UserTables { get; set; }
        public List<UserField> UserFields { get; set; }
        public List<dynamic> UserObjects { get; set; }

    }

    public class UserTable
    {
        public string TableDescription { get; set; }
        public string TableName { get; set; }
        public string TableType { get; set; }
    }

    public class UserField
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public string SubType { get; set; }
        public string TableName { get; set; }
        public string Type { get; set; }
        public int? Size { get; set; }
        public int? EditSize { get; set; }
    }

    public class ODataSAPResponse
    {
        public List<UserField> value { get; set; }
    }
}
