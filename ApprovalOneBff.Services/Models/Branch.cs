namespace ApprovalOneBff.Services.Models
{
  public class Branch
  {
    public int BranchId { get; set; }
    public string BranchName { get; set; }
  }
}