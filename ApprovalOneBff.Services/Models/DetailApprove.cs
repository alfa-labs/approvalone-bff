using System;

namespace ApprovalOneBff.Services.Models
{
    public class DetailApprove
    {
        private string _date { get; set; }
        private string _hour { get; set; }

        public string Name { get; set; }
        public string Status { get; set; }
        public string Substitute { get; set; }
        public string Hour
        {
            get
            {
                if(String.IsNullOrEmpty(_hour)) return "";
                string hour = $"{_hour.PadLeft(4, '0')}";

                return $"{hour.Substring(0, 2)}:{hour.Substring(2, 2)}";
            }
            set => _hour = value;
        }

        public string Date
        {
            get
            {
                return String.IsNullOrEmpty(_date) ? "" :
                            $"{_date.Substring(3, 2)}" +
                            $"/{_date.Substring(0, 2)}" +
                            $"/{_date.Substring(6, 4)}";
            }
            set => _date = value;
        }
    }
}