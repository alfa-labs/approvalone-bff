﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApprovalOneBff.Services.Models
{
    public class CostCenter
    {
        public string CostingCode { get; set; }
        public string OcrName { get; set; }

        public CostCenter() { }
        public CostCenter(string costingCode, string ocrName)
        {
            this.CostingCode = costingCode;
            this.OcrName = ocrName;
        }

    }
}
