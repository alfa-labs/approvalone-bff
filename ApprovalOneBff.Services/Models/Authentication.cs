﻿using OneServiceLayer.Domain.Login;

namespace ApprovalOneBff.Services.Models
{
    public class Authentication
    {
        public bool Success { get; set; }
        public LoginResponse Data { get; set; }
    }

    public class LoginResponse
    {
        public string SessionId { get; set; }
    }
}
